<?php
//Alright, here we go
/* This program needs to ask the user for an image and several parameters and send that
 * information to the fortran program, which creates images in the Sites/img/ 
 * folder.
 *
 * DON'T FORGET TO UPDATE THE VERSION IN THE SITES FOLDER AFTER MAKING CHANGES
 *
 * This program will work for any of the five enhancement algorithms, as each has a separate php function dedicated to it that must be specified in the POST
 */ 
$id = rand(0,10000); 							//randomized id given to each image to prevent overwriting on server
$alg = $_POST['algorithm']; 						//the enhancement technique to use
$fn = $_FILES['uploadedfile']['name'];
$fn = str_replace(" ","_",$fn);
$fn = str_replace("'","",$fn);	 					//the filename
$ext = pathinfo($_FILES['uploadedfile']['name'], PATHINFO_EXTENSION); 	//the file extension, will always be fits or fit
$fn = substr($fn, 0, strlen($fn)-(strlen($ext)+1)); 			//get rid of the extension from the filename
$tmpext = $_FILES['uploadedfile']['tmp_name']; 				//where the file is currently located
$dir = "/Users/cometweb/Sites/img/"; 					//the directory to which the file will be moved
$sub = "img/";								//subdirectory where images are stored

while(file_exists("${dir}${fn}_unenhanced_${id}.$ext")) { 		//make sure the filename and id have not already been taken
    $id = rand(0,10000);
}
move_uploaded_file("$tmpext", "${dir}${fn}_unenhanced_${id}.$ext"); 	//move the file to /Users/cometweb/Sites/img
$heightmax = 700; 							//set the dimensions for the png images to display
$widthmax = 330;

//man I miss switch statements
if($alg === 'aziav') {
    aziav();
}
if($alg === 'azimed') {
    azimed();
}
if($alg === 'aziren') {
    aziren();
}
if($alg === 'invrho') {
    invrho();
}
if($alg === 'rvsf') {
    rvsf();
}
if($alg === 'mosaic') {
    mosaic();
}



//performs the division by azimuthal average routine
function aziav() {
    global $fn; 					//input image base file name
    global $ext; 					//input image extension
    global $id; 					//id assigned to input image
    global $dir; 					//input image directory. Will always be /Users/cometweb/Sites/img/
    global $sub; 					//subdirectory of sites where the image is. Will always be img/

    //get data from form
    $x = $_POST['x'];
    $y = $_POST['y'];
    $xmin = $_POST['xmin'];
    $ymin = $_POST['ymin'];
    $xmax = $_POST['xmax'];
    $ymax = $_POST['ymax'];
    $raxis = $_POST['raxis'];
    $thaxis = $_POST['thaxis'];
    $sigs = $_POST['reject'];

    //set the file names for the four images
    //these file names are the original name followed by the image type (which corresponds to the original xy,
    // the original converted to rho-theta, the enhanced rho-theta, and the enhanced xy), followed by the id
    $firtn = "{$fn}_unenhanced_{$id}";
    $furtn = "{$fn}_unenhanced_rhotheta_{$id}";
    $fertn = "{$fn}_aziav_enhanced_rhotheta_{$id}";
    $fartn = "{$fn}_aziav_enhanced_{$id}"; //I could not resist


    //create a file that contains the information for the routine
    $infofile = fopen("info.txt",'w');
    fwrite($infofile, $firtn.".$ext\n".$furtn."\n".$fertn."\n".$fartn."\n".$x.",".$y."\n".$xmin.",".$xmax."\n".$ymin.",".$ymax."\n".$raxis."\n".$thaxis."\n".$sigs."\n");
    fclose($infofile);

    //enhance the image
    exec("/Users/cometweb/Sites/cgi-bin/aziaverage.e",$rval);

    // nonzero return value
    if ( $rval[0] ) {
        echo "error:".$rval[0];
        exit(1);
    }


    //Tell the facility the image file names
    echo $sub.$firtn."&".$sub.$furtn."&".$sub.$fertn."&".$sub.$fartn;

    //read all four images in preparation for resizing and conversion to png
    $imgs = NewMagickWand();

    MagickReadImages($imgs, array("{$firtn}.$ext","{$furtn}.fits","{$fertn}.fits","{$fartn}.fits"));
    $fnsPNG = array($fartn.'.png',$fertn.'.png',$furtn.'.png',$firtn.'.png');
    $fnsEXT = array("$fartn.$ext","$fertn.$ext","$furtn.$ext","$firtn.$ext");

    $index = 0;

    global $heightmax;
    global $widthmax;
    $ratio = $widthmax / $heightmax;

    $old_xywidth = $xmax - $xmin;
    $old_xyheight = $ymax - $ymin;
    $xyratio = $old_xywidth / $old_xyheight;

    $old_rtwidth = $thaxis;
    $old_rtheight = $raxis;
    $rtratio = $old_rtwidth / $old_rtheight;
    //set the new dimensions for the xy images, first checking whether it is too wide or too tall
    if($xyratio > $ratio) { //too wide
        $new_xywidth = $widthmax;
        $new_xyheight = $new_xywidth / $xyratio;
    } else { //too tall
        $new_xyheight = $heightmax;
        $new_xywidth = $new_xyheight * $xyratio;
    }
    //do the same for the rho theta images
    if($rtratio > $ratio) {
        $new_rtwidth = $widthmax;
        $new_rtheight = $new_rtwidth / $rtratio;
    } else { //too tall
        $new_rtheight = $heightmax;
        $new_rtwidth = $new_rtheight * $rtratio;
    }

    //goes enhanced, enhanced rhotheta, unenhanced rhotheta, unenhanced

    while(MagickGetNumberImages($imgs) > 0) {
        //MagickWriteImage($imgs, $fnsEXT[$index]);
        MagickSetImageFormat($imgs, 'png');
        if ( $index == 3 ) {
            MagickCropImage($imgs, $old_xywidth, $old_xyheight, $xmin, MagickGetImageHeight($imgs) -  $ymax);
        }
        if ( $index == 0 ) {
            MagickCropImage($imgs, $old_xywidth, $old_xyheight,0,MagickGetImageHeight($imgs) - $old_xyheight );
        }
        if($index % 3 == 0) {
            MagickResizeImage($imgs, $new_xywidth, $new_xyheight, MW_QuadraticFilter, 1.0);
        } else {
            MagickResizeImage($imgs, $new_rtwidth, $new_rtheight, MW_QuadraticFilter, 1.0);
        }
        MagickEqualizeImage($imgs);
        MagickWriteImage($imgs, $fnsPNG[$index]);
        MagickRemoveImage($imgs);
        $index++;
    }

}

// Perform the Division by Azimuthal Median Routine
function azimed() {
    //see aziav() for variable explanations
    global $fn;
    global $ext;
    global $id;
    global $dir;
    global $sub;

    $x = $_POST['x'];
    $y = $_POST['y'];
    $xmin = $_POST['xmin'];
    $ymin = $_POST['ymin'];
    $xmax = $_POST['xmax'];
    $ymax = $_POST['ymax'];
    $raxis = $_POST['raxis'];
    $thaxis = $_POST['thaxis'];

    //Set the file names
    $firtn = "{$fn}_unenhanced_{$id}";
    $furtn = "{$fn}_unenhanced_rhotheta_{$id}";
    $fertn = "{$fn}_azimed_enhanced_rhotheta_{$id}";
    $fartn = "{$fn}_azimed_enhanced_{$id}"; //I could not resist


    //Write a file with the appropriate information for the enhancement routine
    $infofile = fopen("info.txt",'w');
    fwrite($infofile, $firtn.".$ext\n".$furtn."\n".$fertn."\n".$fartn."\n".$x.",".$y."\n".$xmin.",".$xmax."\n".$ymin.",".$ymax."\n".$raxis."\n".$thaxis."\n");
    fclose($infofile);

    //Run the enhancement routine
    exec("/Users/cometweb/Sites/cgi-bin/azimed.e",$rval);
    // nonzero return value
    if ( $rval[0] ) {
        echo "error";
        exit(1);
    }

    //Tell the facility the image file names
    echo $sub.$firtn."&".$sub.$furtn."&".$sub.$fertn."&".$sub.$fartn;

    //Read the images into a magickwand to convert to png and resize
    $imgs = NewMagickWand();
    MagickReadImages($imgs, array("{$firtn}.$ext","{$furtn}.fits","{$fertn}.fits","{$fartn}.fits"));
    $fnsPNG = array($fartn.'.png',$fertn.'.png',$furtn.'.png',$firtn.'.png');
    $fnsEXT = array("$fartn.$ext","$fertn.$ext","$furtn.$ext","$firtn.$ext");

    $index = 0;

    global $heightmax;
    global $widthmax;
    $ratio = $widthmax / $heightmax;

    $old_xywidth = $xmax - $xmin;
    $old_xyheight = $ymax - $ymin;
    $xyratio = $old_xywidth / $old_xyheight;

    $old_rtwidth = $thaxis;
    $old_rtheight = $raxis;
    $rtratio = $old_rtwidth / $old_rtheight;
    //set the new dimensions for the xy images, first checking whether it is too wide or too tall
    if($xyratio > $ratio) { //too wide
        $new_xywidth = $widthmax;
        $new_xyheight = $new_xywidth / $xyratio;
    } else { //too tall
        $new_xyheight = $heightmax;
        $new_xywidth = $new_xyheight * $xyratio;
    }
    //do the same for the rho theta images
    if($rtratio > $ratio) {
        $new_rtwidth = $widthmax;
        $new_rtheight = $new_rtwidth / $rtratio;
    } else { //too tall
        $new_rtheight = $heightmax;
        $new_rtwidth = $new_rtheight * $rtratio;
    }

    //goes enhanced, enhanced rhotheta, unenhanced rhotheta, unenhanced

    while(MagickGetNumberImages($imgs) > 0) {
        //MagickWriteImage($imgs, $fnsEXT[$index]);
        MagickSetImageFormat($imgs, 'png');
        if ( $index == 3 ) {
            MagickCropImage($imgs, $old_xywidth, $old_xyheight,$xmin, MagickGetImageHeight($imgs) -  $ymax);
        }
        if ( $index == 0 ) {
            MagickCropImage($imgs, $old_xywidth, $old_xyheight,0,MagickGetImageHeight($imgs) - $old_xyheight );
        }
        if($index % 3 == 0) {
            MagickResizeImage($imgs, $new_xywidth, $new_xyheight, MW_QuadraticFilter, 1.0);
        } else {
            MagickResizeImage($imgs, $new_rtwidth, $new_rtheight, MW_QuadraticFilter, 1.0);
        }
        MagickEqualizeImage($imgs);
        MagickWriteImage($imgs, $fnsPNG[$index]);
        MagickRemoveImage($imgs);
        $index++;
    }


}

//Do the Azimuthal Renormalization routine
function aziren() {
    //see aziav for the explanation of variables
    global $fn;
    global $ext;
    global $id;
    global $sub;

    $x = $_POST['x'];
    $y = $_POST['y'];
    $xmin = $_POST['xmin'];
    $ymin = $_POST['ymin'];
    $xmax = $_POST['xmax'];
    $ymax = $_POST['ymax'];
    $raxis = $_POST['raxis'];
    $thaxis = $_POST['thaxis'];
    $sigs = $_POST['reject'];
    $renorm = $_POST['renorm'];

    //Prepare the file names
    $firtn = "{$fn}_unenhanced_{$id}";
    $furtn = "{$fn}_unenhanced_rhotheta_{$id}";
    $fertn = "{$fn}_aziren_enhanced_rhotheta_{$id}";
    $fartn = "{$fn}_aziren_enhanced_{$id}"; //I could not resist


    $infofile = fopen("info.txt",'w');
    fwrite($infofile, $firtn.".$ext\n".$furtn."\n".$fertn."\n".$fartn."\n".$x.",".$y."\n".$xmin.",".$xmax."\n".$ymin.",".$ymax."\n".$raxis."\n".$thaxis."\n".$sigs."\n".$renorm."\n");

    //Run the routine
    exec("/Users/cometweb/Sites/cgi-bin/aziren.e",$rval);

    // nonzero return value
    if ( $rval[0] ) {
        echo "error";
        exit(1);
    }

    //Tell the facility the image file names
    echo $sub.$firtn."&".$sub.$furtn."&".$sub.$fertn."&".$sub.$fartn;
    //Read the images into a MagickWand in preparation for conversion to PNG and resizing
    $imgs = NewMagickWand();
    MagickReadImages($imgs, array("{$firtn}.$ext","{$furtn}.fits","{$fertn}.fits","{$fartn}.fits"));
    $fnsPNG = array($fartn.'.png',$fertn.'.png',$furtn.'.png',$firtn.'.png');
    $fnsEXT = array("$fartn.$ext","$fertn.$ext","$furtn.$ext","$firtn.$ext");

    $index = 0;

    global $heightmax;
    global $widthmax;
    $ratio = $widthmax / $heightmax;

    $old_xywidth = $xmax - $xmin;
    $old_xyheight = $ymax - $ymin;
    $xyratio = $old_xywidth / $old_xyheight;

    $old_rtwidth = $thaxis;
    $old_rtheight = $raxis;
    $rtratio = $old_rtwidth / $old_rtheight;
    //set the new dimensions for the xy images, first checking whether it is too wide or too tall
    if($xyratio > $ratio) { //too wide
        $new_xywidth = $widthmax;
        $new_xyheight = $new_xywidth / $xyratio;
    } else { //too tall
        $new_xyheight = $heightmax;
        $new_xywidth = $new_xyheight * $xyratio;
    }
    //do the same for the rho theta images
    if($rtratio > $ratio) {
        $new_rtwidth = $widthmax;
        $new_rtheight = $new_rtwidth / $rtratio;
    } else { //too tall
        $new_rtheight = $heightmax;
        $new_rtwidth = $new_rtheight * $rtratio;
    }

    //goes enhanced, enhanced rhotheta, unenhanced rhotheta, unenhanced

    while(MagickGetNumberImages($imgs) > 0) {
        //MagickWriteImage($imgs, $fnsEXT[$index]);
        MagickSetImageFormat($imgs, 'png');
        if ( $index == 3 ) {
            MagickCropImage($imgs, $old_xywidth, $old_xyheight,$xmin, MagickGetImageHeight($imgs) -  $ymax);
        }
        if ( $index == 0 ) {
            MagickCropImage($imgs, $old_xywidth, $old_xyheight,0,MagickGetImageHeight($imgs) - $old_xyheight );
        }
        if($index % 3 == 0) {
            MagickResizeImage($imgs, $new_xywidth, $new_xyheight, MW_QuadraticFilter, 1.0);
        } else {
            MagickResizeImage($imgs, $new_rtwidth, $new_rtheight, MW_QuadraticFilter, 1.0);
        }
        MagickEqualizeImage($imgs);
        MagickWriteImage($imgs, $fnsPNG[$index]);
        MagickRemoveImage($imgs);
        $index++;
    }




}
//Perform the Division by 1/rho routine
function invrho() {
    //see aziav() for an explanation of the variables
    global $fn;
    global $ext;
    global $id;
    global $sub;

    $x = $_POST['x'];
    $y = $_POST['y'];
    $xmin = $_POST['xmin'];
    $ymin = $_POST['ymin'];
    $xmax = $_POST['xmax'];
    $ymax = $_POST['ymax'];
    $raxis = $_POST['raxis'];
    $thaxis = $_POST['thaxis'];

    //Prepare the file names
    $firtn = "{$fn}_unenhanced_{$id}";
    $furtn = "{$fn}_unenhanced_rhotheta_{$id}";
    $fertn = "{$fn}_invrho_enhanced_rhotheta_{$id}";
    $fartn = "{$fn}_invrho_enhanced_{$id}"; //I could not resist


    //Write the information into a file so the routine can read it
    $infofile = fopen("info.txt",'w');
    fwrite($infofile, $firtn.".$ext\n".$furtn."\n".$fertn."\n".$fartn."\n".$x.",".$y."\n".$xmin.",".$xmax."\n".$ymin.",".$ymax."\n".$raxis."\n".$thaxis."\n");

    //Run the routine
    exec("/Users/cometweb/Sites/cgi-bin/invrho.e",$rval);	

    // nonzero return value
    if ( $rval[0] ) {
        echo "error";
        exit(1);
    }

    //Tell the facility the image file names
    echo $sub.$firtn."&".$sub.$furtn."&".$sub.$fertn."&".$sub.$fartn;

    //Read the images into a MagickWand in preparation for converstion to PNG and resizing
    $imgs = NewMagickWand();
    MagickReadImages($imgs, array("{$firtn}.$ext","{$furtn}.fits","{$fertn}.fits","{$fartn}.fits"));
    $fnsPNG = array($fartn.'.png',$fertn.'.png',$furtn.'.png',$firtn.'.png');
    $fnsEXT = array("$fartn.$ext","$fertn.$ext","$furtn.$ext","$firtn.$ext");

    $index = 0;

    global $heightmax;
    global $widthmax;
    $ratio = $widthmax / $heightmax;

    $old_xywidth = $xmax - $xmin;
    $old_xyheight = $ymax - $ymin;
    $xyratio = $old_xywidth / $old_xyheight;

    $old_rtwidth = $thaxis;
    $old_rtheight = $raxis;
    $rtratio = $old_rtwidth / $old_rtheight;
    //set the new dimensions for the xy images, first checking whether it is too wide or too tall
    if($xyratio > $ratio) { //too wide
        $new_xywidth = $widthmax;
        $new_xyheight = $new_xywidth / $xyratio;
    } else { //too tall
        $new_xyheight = $heightmax;
        $new_xywidth = $new_xyheight * $xyratio;
    }
    //do the same for the rho theta images
    if($rtratio > $ratio) {
        $new_rtwidth = $widthmax;
        $new_rtheight = $new_rtwidth / $rtratio;
    } else { //too tall
        $new_rtheight = $heightmax;
        $new_rtwidth = $new_rtheight * $rtratio;
    }

    //goes enhanced, enhanced rhotheta, unenhanced rhotheta, unenhanced

    while(MagickGetNumberImages($imgs) > 0) {
        //MagickWriteImage($imgs, $fnsEXT[$index]);
        MagickSetImageFormat($imgs, 'png');
        if ( $index == 3 ) {
            MagickCropImage($imgs, $old_xywidth, $old_xyheight,$xmin, MagickGetImageHeight($imgs) -  $ymax);
        }
        if ( $index == 0 ) {
            MagickCropImage($imgs, $old_xywidth, $old_xyheight,0,MagickGetImageHeight($imgs) - $old_xyheight );
        }
        if($index % 3 == 0) {
            MagickResizeImage($imgs, $new_xywidth, $new_xyheight, MW_QuadraticFilter, 1.0);
        } else {
            MagickResizeImage($imgs, $new_rtwidth, $new_rtheight, MW_QuadraticFilter, 1.0);
        }
        MagickEqualizeImage($imgs);
        MagickWriteImage($imgs, $fnsPNG[$index]);
        MagickRemoveImage($imgs);
        $index++;
    }


}

//Perform the Radially Variable Spatial Filtering routine
function rvsf() {
    //see aziav() for variable descriptions
    global $fn;
    global $ext;
    global $id;
    global $sub;

    $log10 = $_POST['log10'];
    $x = $_POST['x'];
    $y = $_POST['y'];
    $xmin = $_POST['xmin'];
    $ymin = $_POST['ymin'];
    $xmax = $_POST['xmax'];
    $ymax = $_POST['ymax'];
    $kernelA = $_POST['kernelA'];
    $kernelB = $_POST['kernelB'];
    $kernelN = $_POST['kernelN'];

    $x = $x + $xmin;
    $y = $y + $ymin;

    //RVSF only produces two images: the input and the enhanced image
    $firtn = "{$fn}_unenhanced_{$id}";
    
    $cleanN = str_replace(".","-",$kernelN);
    $fartn = "{$fn}_rvsf_enhanced_a{$kernelA}_b{$kernelB}_n{$cleanN}_{$id}"; //I could not resist

    //Write a file with the information for the routine to access.
    $infofile = fopen("info.txt",'w');
    fwrite($infofile, $firtn.".$ext\n".$log10."\n".$fartn."\n".$x.",".$y."\n".$xmin.",".$xmax."\n".$ymin.",".$ymax."\n".$kernelA."\n".$kernelB."\n".$kernelN."\n");

    //run the routine
    exec("/Users/cometweb/Sites/cgi-bin/rvsf.e rvsf",$rval);

    // nonzero return value
    if ( $rval[0] ) {
        echo "$rval[0]";
        exit(1);
    }

    //Tell the facility the image file names
    echo $sub.$firtn."&".$sub.$fartn;

    //read the images into a MagickWand in preparation for conversion to PNG and resizing	
    $imgs = NewMagickWand();
    MagickReadImages($imgs, array("{$firtn}.$ext","{$fartn}.fits"));

    $fnsPNG = array($fartn.'.png',$firtn.'.png');
    $fnsEXT = array("$fartn.$ext","$firtn.$ext");
    $index = 0;
    global $heightmax;
    global $widthmax;
    $ratio = $widthmax / $heightmax;



    $old_xywidth = $xmax - $xmin;
    $old_xyheight = $ymax - $ymin;
    $xyratio = $old_xywidth / $old_xyheight;

    //set the new dimensions for the xy images, first checking whether it is too wide or too tall
    if($xyratio > $ratio) { //too wide
        $new_xywidth = $widthmax;
        $new_xyheight = $new_xywidth / $xyratio;
    } else { //too tall
        $new_xyheight = $heightmax;
        $new_xywidth = $new_xyheight * $xyratio;
    }


    //goes enhanced, unenhanced

    while(MagickGetNumberImages($imgs) > 0) {
        // MagickWriteImage($imgs, $fnsEXT[$index]);
        MagickSetImageFormat($imgs, 'png');
        if ( $index == 1 ) {
            MagickCropImage($imgs, $old_xywidth, $old_xyheight,$xmin, MagickGetImageHeight($imgs) -  $ymax);

        }
        if ( $index == 0 ) {
            MagickCropImage($imgs, $old_xywidth, $old_xyheight,0,MagickGetImageHeight($imgs) - $old_xyheight );
        }

        MagickResizeImage($imgs, $new_xywidth, $new_xyheight, MW_QuadraticFilter, 1.0);

        //		MagickEqualizeImage($imgs);
        MagickEqualizeImage($imgs);
        MagickWriteImage($imgs, $fnsPNG[$index]);
        MagickRemoveImage($imgs);
        $index++;
    }





}

function mosaic() {
    //see aziav() for variable descriptions
    global $fn;
    global $ext;
    global $id;
    global $sub;

    $log10 = $_POST['log10'];
    $x = $_POST['x'];
    $y = $_POST['y'];
    $xmin = $_POST['xmin'];
    $ymin = $_POST['ymin'];
    $xmax = $_POST['xmax'];
    $ymax = $_POST['ymax'];
    $kernelA1 = $_POST['kernelA1'];
    $kernelA2 = $_POST['kernelA2'];
    $kernelB1 = $_POST['kernelB1'];
    $kernelB2 = $_POST['kernelB2'];
    $kernelN1 = $_POST['kernelN1'];
    $kernelN2 = $_POST['kernelN2'];

    $kernelA = array(
            0 => $kernelA1,
            1 => $kernelA2
            );
    $kernelB = array(
            0 => $kernelB1,
            1 => $kernelB2
            );
    $kernelN = array(
            0 => $kernelN1,
            1 => $kernelN2
            );

    //The mosaic produces eight images: one for each combination of kernel terms
    $firtn = "{$fn}_unenhanced_{$id}";
    $cleanN1 = str_replace(".","-",$kernelN1);
    $cleanN2 = str_replace(".","-",$kernelN2);
    $fartn = array("{$fn}_rvsf_enhanced_a{$kernelA1}_b{$kernelB1}_n{$cleanN1}_{$id}", "{$fn}_rvsf_enhanced_a{$kernelA1}_b{$kernelB1}_n{$cleanN2}_{$id}", "{$fn}_rvsf_enhanced_a{$kernelA1}_b{$kernelB2}_n{$cleanN1}_{$id}", "{$fn}_rvsf_enhanced_a{$kernelA1}_b{$kernelB2}_n{$cleanN2}_{$id}", "{$fn}_rvsf_enhanced_a{$kernelA2}_b{$kernelB1}_n{$cleanN1}_{$id}", "{$fn}_rvsf_enhanced_a{$kernelA2}_b{$kernelB1}_n{$cleanN2}_{$id}", "{$fn}_rvsf_enhanced_a{$kernelA2}_b{$kernelB2}_n{$cleanN1}_{$id}", "{$fn}_rvsf_enhanced_a{$kernelA2}_b{$kernelB2}_n{$cleanN2}_{$id}");
    $imgs = NewMagickWand();


    for ($a = 0; $a<2; $a++) {
        for ($b = 0; $b<2; $b++) {
            for($n = 0; $n<2;$n++) {
                //Write a file with the information for the routine to access.
                $infofile = fopen("info.txt",'w');
                fwrite($infofile, $firtn.".$ext\n".$log10."\n".$fartn[4*$a+2*$b+$n]."\n".$x.",".$y."\n".$xmin.",".$xmax."\n".$ymin.",".$ymax."\n".$kernelA[$a]."\n".$kernelB[$b]."\n".$kernelN[$n]."\n");

                //run the routine
                exec("/Users/cometweb/Sites/cgi-bin/rvsf.e mosaic".(4*$a+2*$b+$n+1),$rval);

                // nonzero return value
                if ( $rval[0] ) {
                    echo "error";
                    exit(1);
                }



                //read the images into a MagickWand in preparation for conversion to PNG and resizing

                MagickReadImages($imgs, array("{$fartn[4*$a+2*$b+$n]}.fits"));

                $fnsPNG = array($fartn[4*$a+2*$b+$n].'.png',);
                $fnsEXT = array("${fartn[4*$a+2*$b+$n]}.$ext");
                $index = 0;

                global $heightmax;
                global $widthmax;
                $ratio = $widthmax / $heightmax;

                $xywidth = $xmax - $xmin;
                $xyheight = $ymax - $ymin;
                $xyratio = $xywidth / $xyheight;

                //set the new dimensions for the xy images, first checking whether it is too wide or too tall
                if($xyratio > $ratio) { //too wide
                    $xywidth = $widthmax;
                    $xyheight = $xywidth / $xyratio;
                } else { //too tall
                    $xyheight = $heightmax;
                    $xywidth = $xyheight * $xyratio;
                }

                //goes enhanced
                while(MagickGetNumberImages($imgs) > 0) {
                    //MagickWriteImage($imgs, $fnsEXT[$index]);
                    MagickSetImageFormat($imgs, 'png');
                    MagickResizeImage($imgs, $xywidth, $xyheight, MW_QuadraticFilter, 1.0);
                    MagickEqualizeImage($imgs);
                    MagickWriteImage($imgs, $fnsPNG[$index]);
                    MagickRemoveImage($imgs);
                    $index++;
                }
            }
        }
    }

    //Tell the facility the image file names
    $return = "";
    for($i = 0; $i < 8; $i++ ) {
        $return .= $sub.$fartn[$i];
        if($i != 7) {
            $return .= "&";
        }
    }
    echo $return;




}
?>
