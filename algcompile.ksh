#!/bin/ksh
if [ "$#" -lt 2 ]
then
	echo "Usage: $0 executable_name.e source_code_name.f"
	exit 1
fi
gfortran -o $1 $2 /users/cometweb/iraf/fitsiowrap.o /opt/local/lib/libcfitsio.a

if [ "$#" -eq 3 ] #third argument is aziav, azimed, aziren, invrho, or rvsf
then

	if [ $3 = "aziav" ]
	then
		cp $1 ~/Sites/div_by_azimuthal_average.exe
		cp $2 ~/Sites/div_by_azimuthal_average.f
		mv ~/cgi-bin/div_by_azimuthal_average.exe ~/cgi-bin/old/
		cp $1 ~/cgi-bin/div_by_azimuthal_average.exe
	fi

	if [ $3 = "azimed" ]
	then
		cp $1 ~/Sites/div_by_azimuthal_median.exe
		cp $2 ~/Sites/div_by_azimuthal_median.f
		mv ~/cgi-bin/div_by_azimuthal_median.exe ~/cgi-bin/old/
		cp $1 ~/cgi-bin/div_by_azimuthal_median.exe
	fi
	if [ $3 = "aziren" ]
	then
		cp $1 ~/Sites/azimuthal_renorm.exe
		cp $2 ~/Sites/azimuthal_renorm.f
		mv ~/cgi-bin/azimuthal_renorm.exe ~/cgi-bin/old/
		cp $1 ~/cgi-bin/azimuthal_renorm.exe
	fi
	if [ $3 = "invrho" ]
	then
		cp $1 ~/Sites/div_by_inv_rho.exe
		cp $2 ~/Sites/div_by_inv_rho.f
		mv ~/cgi-bin/div_by_inv_rho.exe ~/cgi-bin/old/
		cp $1 ~/cgi-bin/div_by_inv_rho.exe
	fi
	if [ $3 = "rvsf" ]
	then
		cp $1 ~/Sites/radially_variable_spatial_filtering.exe
		cp $2 ~/Sites/radially_variable_spatial_filtering.f
		mv ~/cgi-bin/radially_variable_spatial_filtering.exe ~/cgi-bin/old/
		cp $1 ~/cgi-bin/radially_variable_spatial_filtering.exe
	fi
fi
