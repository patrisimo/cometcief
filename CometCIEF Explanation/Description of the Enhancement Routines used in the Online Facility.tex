
\documentclass[12pt]{article}
\usepackage{amsmath}
\usepackage{amsthm,amsfonts,mathtools, amssymb}
\usepackage{graphicx,capt-of,wrapfig,multirow,hhline}
\usepackage[hidelinks]{hyperref} 
\usepackage{enumerate}
\usepackage{ulem}
\usepackage{changepage}
\usepackage[usenames,dvipsnames]{color}
%\usepackage[margin=1in]{geometry}
\textwidth=6.25in
\textheight=9.5in
\headheight=0in
\headsep=.5in
\hoffset=-0.4in
\topmargin -.75in
\rightmargin -.75in

%\addtolength{\leftmargin}{5 in}
\hypersetup{
	urlcolor=blue
}

\newcommand{\lp}{\left(}
\newcommand{\rp}{\right)}
\newcommand{\mcaption}[1]{\textcolor{CornflowerBlue}{\caption{#1}}}
\newcommand{\hyasc}{0.51}
\newcommand{\harsc}{0.114}

\setlength{\parindent}{0.0cm}   % Don't indent the paragraphs

\begin{document}
\title{An In-depth Description of the Enhancement Routines used in this Facility}
\author{M. Patrick Martin}
\maketitle
This online Cometary Coma Image Enhancement Facility provides five different enhancement routines for the users.  Other less sophisticated techniques are available in standard image processing/enhancing packages and are therefore not included here. While the help page offers a short overview of the routines, this write-up is meant to provide an in-depth description of the above five techniques. Additional information on these techniques as well as the pros and cons of different techniques can be found in \href{http://www.sciencedirect.com/science/article/pii/S0019103514002814}{\textcolor{blue}{\emph{Samarasinha \& Larson (2014)}}}\footnote{http://www.sciencedirect.com/science/article/pii/S0019103514002814} and references therein (click \href{http://arxiv.org/abs/1406.0033}{\textcolor{blue}{\emph{here}}}\footnote{http://arxiv.org/abs/1406.0033} for a preprint). We highly recommend users be familiar with the different enhancement techniques to facilitate their proper usage. \\

Unenhanced cometary images suffer from the very aspect that makes comets so amazing to the average person: their brightness distribution.  The structure of the coma is obfuscated by its brightness distribution, and many individual features may not be easily recognizable. This makes analyzing unenhanced cometary images very difficult, as important structural information is contained both near the bright optocenter (the brightest point of the coma) and in the dimmer outer regions of the coma. Enhancement techniques therefore attempt to bring out the structures in both of these areas, through different methods.

\section{Common Information}

Some user-input fields in the web Facility are shared among all enhancement algorithms. The first of these is the file to upload. This needs to be a FITS image file (with .fits or .fit extension) of the comet you wish to enhance. \\

Second is the (x,y) coordinates of the nucleus/optocenter. This is the brightest point of the comet, which sometimes is nearly in the middle of the coma, but that is not guaranteed. You can find the optocenter (nucleus) by adjusting the brightness floor of the image until there are only a few pixels that are highlighted -- these are the brightest pixels of the comet in the image, so choose the one with the highest brightness. Some programs (like IRAF's imcntr task) are capable of finding the optocenter given a point reasonably close to it.\\
\begin{wrapfigure}{r}{0.6\textwidth}
%  \vspace{-20pt}
     \includegraphics[scale=\hyasc]{unenhanced1.png}
     \includegraphics[scale=\harsc]{unenhanced2.png}
   \mcaption{The unenhanced images used in later figures. Left image is from D. Schleicher and L. Woodney.\label{fig:unenhanced}}
   \vspace{-10pt}
\end{wrapfigure}
Third is the region of the image to enhance. This is set by default to be the entire image, but may be scaled down. Large images take longer to enhance, and if there's only a specific region you're interested in, then you may wish to restrict the routines to that region. Do note, however, that restricting the region in this manner may alter the optocenter's (x,y) coordinates in the new region.\\

Further, when using the Radially Variable Spatial Filtering method, you may have to select the three kernel terms by trial and error to find the values that work best with your image. \\
\\
Each image must be less than 10.8 megapixels in size, with no dimension larger than 4,096 pixels, although these limits can be subverted by choosing a region to enhance that meets these requirements. However, the file size limit of 20MB cannot be bypassed in this manner (cropping the image before uploading it will reduce the file size).\\

To help the explanations, we will provide the outcomes from different enhancement algorithms of the unenhanced images, shown in Figure~\ref{fig:unenhanced}. \\

All of the routines will add keywords to the image headers of the output images indicating the relevant parameters chosen.
\section{Division by Azimuthal Average}
\begin{wrapfigure}{H}{0.59\linewidth}
\vspace{-0pt}
  \includegraphics{aziav_desc.png}
  \mcaption{Visual description of the parameters relevant to Division by Azimuthal Average.\label{fig:aziav_desc}}
  \vspace{-15pt}
\end{wrapfigure}
Division by Azimuthal Average works by dividing each pixel's brightness by the average brightness at that radial distance from the nucleus. To do this, the image is first converted into $\rho$-$\theta$ (polar) coordinates with the optocenter at the origin -- this is the ``Unenhanced $\rho$-$\theta$ image'' in the output. Moving along the vertical axis, $\rho$, moves you radially away from the optocenter, while moving horizontally moves you around the optocenter in a circle, the azimuth. In this format, each row of pixels represents an azimuth; however, because a given ($\rho$,$\theta$) pixel likely does not exactly correspond to an (x,y) pixel in the original x-y image, each pixel is divided into 100 (10x10) subpixels, which are used to determine the value of those pixels. The size of these $\rho$-$\theta$ images is determined by the values for the $\rho$- and $\theta$-axes that the user specifies. To assist in choosing these values, the program calculates the radii and circumferences of the largest circle completely contained in the image and of the largest circle partially contained in the image (see Figure~\ref{fig:aziav_desc}). For practical reasons, the $\rho$-axis value cannot exceed the radius of the largest circle partially contained in the image. The $\theta$-axis has a set maximum of 3600 pixels for image size reasons; this can be larger than the circumference of the largest circle partially contained in the image because each azimuth's pixel values are spread across the $\theta$-axis anyway. For example, even though there are four times as many (x,y) pixels in the azimuth when $\rho$=40 than when $\rho$=10, both azimuths must spread across the entire $\theta$ range. \\




 This routine then finds the average value of each row, excluding pixels that are outliers -- these can be stars, cosmic rays, or other imperfections in the image -- and divides each pixel's brightness value by that average. Notice that the ``spreading'' done to fit pixels at each $\rho$ value along the $\theta$-axis does not affect the average, as all pixels for a given $\rho$ are spread equally. Once all the pixels have been adjusted, the resultant image is the ``Enhanced $\rho$-$\theta$ image'' in the output. Finally, this is reconverted to rectangular (x,y) coordinates to create the ``Enhanced x-y image'' in the output.\\
\begin{wrapfigure}{h}{0.6\textwidth}
	\vspace{-20pt}
     \includegraphics[scale=\hyasc]{aziav1.png}
     \includegraphics[scale=\harsc]{aziav2.png}
   \mcaption{Comet images enhanced by Division by Azimuthal Average. The black background represents pixels that were excluded during the conversion to $\rho$-$\theta$ coordinates.\label{fig:aziav}}
   \vspace{-70pt}
\end{wrapfigure} 
Unless the $\rho$-axis value is set to be the radius of the largest circle partially contained in the image, there will be pixels in the initial image that are not considered in the routine. This shows up in the final enhanced image as a circle of enhanced pixels surrounded by zero-valued pixels (e.g. Figure~\ref{fig:aziav}). This distinct boundary may not show up immediately in the output PNG images displayed on the website, rather you may have to download the FITS image and adjust the brightness and contrast settings. \\\\

Even though the Division by Azimuthal Average routine excludes outlier pixels, it still has weaknesses when it comes to images with many stars or imperfections. This is especially problematic when an image has imperfections along the edge. Restricting the region of the image to enhance (to exclude those imperfections) can mitigate this. 
\pagebreak
\section{Division by Azimuthal Median}

\begin{wrapfigure}{r}{0.6\textwidth}
  \vspace{-10pt}
     \includegraphics[scale=\hyasc]{azimed1.png}
     \includegraphics[scale=\harsc]{azimed2.png}
   \mcaption{Comet images enhanced using Division by Azimuthal Median. Note the similarity to the enhanced images in Figure~\ref{fig:aziav}.\label{fig:azimed}}
   \vspace{-5pt}
\end{wrapfigure} 

Division by Azimuthal Median works by dividing each pixel's brightness by the median brightness at that radial distance from the nucleus. The routine is very similar to Division by Azimuthal Average, with the only differences being what value is used to divide the pixels at a given $\rho$, and there is no pixel exclusion in Division by Azimuthal Median. This is because finding the median is much less susceptible to outliers than finding the average. Therefore, Division by Azimuthal Median is preferred over Division by Azimuthal Average for images with many stars or imperfections. In practice, however, there is no significant difference in enhanced images produced by these two routines; compare Figures \ref{fig:aziav} and \ref{fig:azimed} to see the output of both routines on the same images.

\section{Azimuthal Renormalization}

\begin{wrapfigure}{r}{0.6\textwidth}
  \vspace{-15pt}
     \includegraphics[scale=\hyasc]{aziren1.png}
     \includegraphics[scale=\harsc]{aziren2.png}
   \mcaption{Comet images enhanced using Azimuthal Renormalization.\label{fig:aziren}}
   \vspace{-5pt}
\end{wrapfigure} 

Azimuthal Renormalization adjusts the brightness of pixels based on the maximally bright and minimally bright pixels along the azimuth for a given radial distance from the nucleus. Similarly to the last two routines, it converts the image to $\rho$-$\theta$ coordinates, but instead of calculating the mean or median, it finds the maximum and minimum pixel values. Because this is very susceptible to outliers on both ends, the routine disregards any pixels that are outside of some number of standard deviations (e.g. 3) from the mean. Then, using these values, it scales the brightness of each pixel along the azimuth relative to the brightness of these two values. This means that for each different $\rho$, non-outlier pixels will all have the same brightness range. \\

This routine as well as Division by Azimuthal Median and Division by Azimuthal Average are all qualitatively similar enhancement techniques; Azimuthal Renormalization can be thought of as ``Division by Azimuthal Range'', which is precisely the case when the minimum non-outlier brightness in the azimuth is 0.  Compare Figures~\ref{fig:aziav}, \ref{fig:azimed} and \ref{fig:aziren} to see the differences in the three. The question then arises: when should one technique be used over another? \\

In general, Division by Azimuthal Median has a clear advantage in that it is not as susceptible to outliers as the Average or Range -- using this technique doesn't even require you to specify a rejection sigma -- and Division by Azimuthal Average is less susceptible than Azimuthal Renormalization. 


\section{Division by 1/$\rho$}

\begin{wrapfigure}{r}{0.6\textwidth}
  \vspace{-10pt}
     \includegraphics[scale=\hyasc]{invrho1.png}
     \includegraphics[scale=\harsc]{invrho2.png}
   \mcaption{Comet images enhanced using Division by $1/\rho$.\label{fig:invrho}}
   \vspace{-5pt}
\end{wrapfigure} 

Division by 1/$\rho$, where $\rho$ is the cometocentric distance in the sky plane, takes advantage of the property that comets are brightest at the optocenter, and dimmer further away. To counteract this, it multiplies the brightness of each pixel by its distance from the optocenter -- i.e. it divides by the inverse $\rho$, which means the same mathematically. This wording, however, is preferred because brightness decays as 1/$\rho$ in an ideal spherically symmetric uniform outflow, which can be used to roughly model the brightness distribution of dust in cometary comae, but not the brightness distribution of gas species.  Because of this, gas images will not be properly enhanced by this routine, instead yielding an undesirable outcome, as seen in Figure~\ref{fig:invrho} (right). This image is a CN image, and as such cannot be modeled with a 1/$\rho$ profile. The left image in Figure~\ref{fig:invrho} shows the outcome of the routine when applied to the dust (continuum) image.\\

This routine produces a characteristic ``hole" around the optocenter, which is visible in the left image of Figure~\ref{fig:invrho}. This is caused by the multiplication of the optocenter brightness by $\rho$=0 at the optocenter. \\

While this routine requires input values for the number of pixels in the $\rho$- and $\theta$-axes, the entire region of the image will be enhanced regardless of the values chosen. This is because, unlike the previous routines that work by altering a pixel's values based on all the pixels at a given radial distance, this routine only looks at a pixel's distance from the nucleus/optocenter. However, these numbers of pixels in the $\rho$- and $\theta$- axes do impact the size of the unenhanced and enhanced $\rho$-$\theta$ images. If those two images are not important to you, feel free to use the default values.\\

Additionally, because this routine enhances each pixel individually, as opposed to comparing them to surrounding pixels like the earlier routines, this routine is unaffected by outliers. 

\section{Radially Variable Spatial Filtering}

Radially Variable Spatial Filtering is based on the property that features get larger the further they are from the optocenter, which occurs due to velocity dispersions in the coma. This routine adjusts the brightness value of each pixel by considering pixels on the boundary of a region, hereafter called the kernel, around that pixel. The kernel grows larger as it moves away from the optocenter, which provides a finer filter when working in the inner coma, where features are smaller, and a coarser one when in the outer coma, where features are larger.\\

For each pixel, this routine creates a square having a certain kernel width centered at that target pixel with 8 relevant pixels on that square: the four corners, and the four midpoints for the edges (Figure~\ref{fig:rvsf_desc}). Then the target pixel's brightness value is adjusted by the following formula:
\begin{align*}
I_{out} (x,y)=1024  \cdot I(x,y)- 192 \lp\sum I_{ep} \rp- 64\lp\sum I_{cp}\rp
\end{align*}
\begin{wrapfigure}{r}{0.6\linewidth}
  \vspace{-15pt}
  \includegraphics{rvsf_desc.png}
  \mcaption{Visual description of some parameters required for running Radially Variable Spatial Filtering.\label{fig:rvsf_desc}}
  \vspace{-80pt}
\end{wrapfigure}
where  $I_{out}, I(x,y), I_{ep},$ and $I_{cp}$ are the brightness values of the output pixel, the input pixel, an edge pixel, and a corner pixel respectively. To understand why this is useful, consider a region with flat (or near flat) brightness. In this case, $I(x,y)=I_{ep}=I_{cp}$, so the output value is

\begin{align*}
I_{out} (x,y)&= 1024 \cdot I(x,y) \\
	&\hspace{20pt}- 192 (4\cdot I(x,y)) \\
	&\hspace{20pt}- 64(4\cdot I(x,y)) \\
	& = 1024 \cdot I(x,y) \\
	&\hspace{20pt} - 1024 \cdot I(x,y) \\
	&=0
\end{align*}

which means that pixels with similar brightness levels to their surroundings will be darkened, while those that are different will stand out, with the brighter values being those highlighted. \\

The Facility also provides the option to take the logarithm of each pixel value before applying the above formula. If the image contains any negative pixel values, the logarithm will obviously fail, and thus the presence of any negative pixel values will prevent the ``log option" from being applied. To avoid this, one should ensure there are no negative pixels; one possible remedy is to uniformly increase all the pixel values by an amount larger (in absolute value) than the most negative pixel value. The header of the output enhanced image will reflect whether the logarithm was taken or not.\\

To run this routine, one needs kernel values $A$, $B$, and $N$ to determine how the kernel grows as it moves away from the optocenter.  The width of the kernel, $2a$, is computed using these values and $\rho$, the distance from the optocenter to the current pixel, according to the formula

\[a=A+B\cdot\rho^N\]

This means that $A$, $B$, and $N$ affect the size of the kernel in different ways. Close to the nucleus/optocenter, $(B\cdot\rho^N)$ is not very large, so increasing the $A$ term significantly increases the size of the kernel. As $\rho$ increases, however, $A$'s contribution to the kernel width is quickly overshadowed by the exponential term. \\

\begin{wrapfigure}{r}{0.6\textwidth}
  \vspace{-20pt}
     \includegraphics[scale=\hyasc]{rvsf1.png}
     \includegraphics[scale=\harsc]{rvsf2.png}
   \mcaption{Comet images enhanced using Radially Variable Spatial Filtering. The left image used $A=0.2$, $B=2$, $N=0.3$ with log option; in the right image $A=5$, $B=5$, $N=0.3$ without log option.\label{fig:rvsf}}
   \vspace{-10pt}
\end{wrapfigure} 

The $N$ term, on the other hand, is an exponential term, which means that its influence grows as $\rho$ increases; so increasing $N$ provides the greatest effect on the size of the kernel in the outer coma. Closer to the nucleus/optocenter, $\rho^N$ does not change much with changes in $N$, as $\rho$ is close to 1. \\

Finally, the $B$ term is a linear term, which impacts the size of the kernel at all values of $\rho$. \\


This allows us to manipulate the size of the kernel at all distances from the optocenter: if the kernel is too large in the outer coma, reduce the kernel $N$ term; if it is too large near the nucleus/optocenter, reduce the kernel $A$ term; and if it is too large overall, reduce the kernel $B$ term.\\


In general, the $A$ and $B$ terms should be on the order of 1 and the $N$ term should be around 0.2; the $N$ term in particular should be less than 1, or else the kernel will grow far beyond the size of the image. However, these values need to be adjusted for each individual image through trial and error. \\

Because this routine only compares each pixel to 8 surrounding pixels, outliers only affect the pixels in a box around them, producing dark shadows of the outliers nearby, which can be seen as the dark streaks in the right image in Figure~\ref{fig:rvsf}. \\
\\
Figure~\ref{fig:rvsfmosaic} shows how varying the values of $A$, $B$, and $N$ affect the resultant image.

\begin{figure}
  \centering
    \mcaption{Image of comet Hale-Bopp enhanced with Radially Variable Spatial Filtering with different values of $A$, $B$, and $N$ chosen.\label{fig:rvsfmosaic}}
  \begin{tabular}[t]{|c|c|c|c|}
    \hhline{~~--}
    \multicolumn{1}{c}{} &  & $B=1$ & $B=4$ \\
    \hline 
    \multirow{2}{*}{\raisebox{-11\height}{$A=1$}} & $N=0.1$& \raisebox{-.5\height}{\includegraphics{rvsf1101.png}} & \raisebox{-.5\height}{\includegraphics{rvsf1401.png}} \\
    \hhline{~---}
    & $N=0.3$ & \raisebox{-.5\height}{\includegraphics{rvsf1103.png}}  & \raisebox{-.5\height}{\includegraphics{rvsf1403.png}} \\
    \hline % the \raisebox on the A=1 and 4 is not the appropriate way to fix this
    \multirow{2}{*}{\raisebox{-11\height}{$A=4$}} & $N=0.1$ & \raisebox{-.5\height}{\includegraphics{rvsf4101.png}} & \raisebox{-.5\height}{\includegraphics{rvsf4401.png}} \\
    \hhline{~---}
    & $N=0.3$ & \raisebox{-.5\height}{\includegraphics{rvsf4103.png}}  & \raisebox{-.5\height}{\includegraphics{rvsf4403.png}} \\
    \hline

  \end{tabular}
\end{figure}



\end{document}