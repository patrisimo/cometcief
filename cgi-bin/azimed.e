#!/bin/ksh
crdr=`dirname $0`
tcsh $crdr/counter.tcsh azimed
echo "-------" >> $crdr/error_log
echo "_______" >> $crdr/actual_errors
$crdr/cometcief_azimedian.e < $crdr/../img/info.txt >> $crdr/error_log 2>> $crdr/actual_errors
echo $?
