This file describes the files required for the CometCIEF facility
to run. The default location for this system (rooted at Sites/) is
/Users/cometweb/

TABLE OF CONTENTS:

1. Sites/
    1.1 WEB FILES
    1.2 EXECUTABLES
    1.3 OTHER IMPORTANT FILES
2. Sites/CometCIEF Tutorial
    2.1 MAIN FILES
    2.2 IMAGES
3. Sites/CometCIEF Explanation
    3.1 MAIN FILES
    3.2 IMAGES
4. Sites/css
5. Sites/img
6. Sites/js
7. Sites/cgi-bin
    7.1 EXECUTABLES
    7.2 INFORMATION


******************************************************************
**********              1.  Sites/              ******************
******************************************************************
This directory requires at least 703 permissions

The Facility is saved on bitbucket using git, and this is the
home directory. You can update to the latest version by doing
    >git pull origin master
and make an update to the website via
    >git add file_to_add
    >git commit -am "Your message here"
    >git push origin master
    
ALWAYS PULL BEFORE MAKING CHANGES

1.1 WEB FILES


acknowledgements.html	<-- HTML page with acknowledgements to the 
                            various organizations and individuals 
                            who helped with this project
enhance.html  		<-- this is the main webpage
techniques.html		<-- Explains the different techniques and allows 
                        for downloads of the executables
contributors.html   <-- Page thanking those who helped with the 
                        facility
enhance.php 		<-- Is a symbolic link/shortcut to the file 
                        in the img/ folder
verifyFile.php 		<-- Gets information about the uploaded file. 
                        Data sent to with (POST). Uses MagickWand.
downloadFile.php	<-- Has the user download a file. Data sent 
                        to with (GET)

Important note: Sites/enhance.php is a symbolic link/shortcut to 
Sites/img/enhance.php.

1.2 EXECUTABLES

cometcief_aziaverage.e
cometcief_aziaverage.f
cometcief_azimedian.e
cometcief_azimedian.f
cometcief_azirenorm.e
cometcief_azirenorm.f
cometcief_inverserho.e
cometcief_inverserho.f
cometcief_rvsf.e
cometcief_rvsf.f

These are also the executables used by the website, and need to be
updated when new versions of the algorithms come out.

1.3 OTHER IMPORTANT FILES

README                  <-- this file
CometCIEF_test.fits     <-- test image for use in the tutorial
cometcief_techniques.zip <- zip file for download containing the 
                            above .e and .f files
removeFile.ksh          <-- cronjob that runs every hour to remove 
                            images (PNG, FIT, FITS) that are older
                            than 1 hour old

******************************************************************
**********     2.  Sites/CometCIEF Tutorial     ******************
******************************************************************


2.1 MAIN FILES

CometCIEF Tutorial.pdf  <-- document giving a brief tutorial on 
                            how to use the site
CometCIEF Tutorial.tex  <-- TeX file for the pdf

2.2 IMAGES

1_filefield.png
2_axisfield.png
3_enhanced.png
4_rhoerror.png
5_newoutput.png
6_rvsp1101.png
7_rvsp4404.png
8_rvsp2404.png
9_rvspmosaic14140104

These images are used in the pdf, so are required to compile the
TeX.


******************************************************************
**********    3.  Sites/CometCIEF Explanation   ******************
******************************************************************

3.1 MAIN FILES

Description of the Enhancement Routines used in the 
    Online Facility.pdf
Description of the Enhancement Routines used in the 
    Online Facility.tex
    
3.2 IMAGES

aziav1.png
aziav2.png
aziav_desc.png
azimed1.png
azimed2.png
aziren1.png
aziren2.png
invrho1.png
invrho2.png
rvsf1.png
rvsf1101.png
rvsf1103.png
rvsf1401.png
rvsf1403.png
rvsf2.png
rvsf4101.png
rvsf4103.png
rvsf4401.png
rvsf4403.png
rvsf_desc.png
unenhanced1.png
unenhanced2.png

******************************************************************
**********              4.  Sites/css           ******************
******************************************************************

enhance.css		<-- The css used on the webpage. This hides divs, 
                    sets text box sizes, and allows for hover-over 
                    help boxes

******************************************************************
**********              5.  Sites/img           ******************
******************************************************************

enhance.php		<-- Creates an info.txt file and runs the 
                    enhancement algorithms. Data sent to with (POST) 
                    from enhance.js. Uses MagickWand.

Uploaded and enhanced images live here. This probably requires at
least 707 permissions

******************************************************************
**********              6.  Sites/js            ******************
******************************************************************

jquery.js		<-- JQuery file, allows the use of JQuery in my 
                    javascript files, like...
enhance.js		<-- JavaScript file for the webpage. Takes care of 
                    just about everything that isn't enhancement.
cookies.js		<-- JavaScript file for the webpage. Creates and 
                    reads cookies in the browser that allow the 
                    user to view previous enhancements.

******************************************************************
**********              7.  Sites/cgi-bin       ******************
******************************************************************

This folder requires at least 703 permissions

7.1 EXECUTABLES

aziaverage.e	<-- Runs (but is NOT) the Division by Azimuthal
                        Average enhancement algorithm with the 
                        information in info.txt
azimed.e		<-- Runs (but is NOT) the Division by Azimuthal 
                    Median enhancement algorithm with the 
                    information in info.txt
aziren.e		<-- Runs (but is NOT) the Azimuthal 
                    Renormalization enhancement algorithm with 
                    the information in info.txt
invrho.e		<-- Runs (but is NOT) the Division by 1/rho 
                    enhancement algorithm with the information 
                    in info.txt
rvsf.e          <-- Runs (but is NOT) the Radially Variable Spatial
                    Filtering enhancement algorithm with the 
                    information in info.txt
                    
This folder also contains symbolic links to the actual algorithm
executables in Sites/. This is somewhat unnecessary, and one could
avoid using them by modifying the above files to call
$crdr/../cometcief* instead.

7.2 INFORMATION

counter.tcsh	<-- Increments the value in count.txt by 1 and 
                    records the algorithm used in trends.txt 
                    each time an algorithm is run
count.txt		<-- How many times an algorithm has been run
                    Requires 006 permissions
trends.txt		<-- Lists the time and type of each algorithm run
                    Requires 006 permissions
error_log       <-- Keeps track of the output of the program
                    Requires 006 permissions




The computer acting as the server requires some third-party tools 
in order to operate correctly:
-PHP5 must be activated
-MagickWand for PHP must be installed. This is an addon to PHP, 
and can be installed from magickwand.org
	-This requires ImageMagick, which can be installed using 
        macports (>sudo port install imagemagick) or from 
        imagemagick.org



How to compile the enhancement programs:
There is a file in the Sites directory call "algcompile.ksh". 
Let ~/path/to/program.f be the program source code, and 
~/path/to/program.e the desired executable name, both with the 
complete path name. 

Run
    >ksh algcompile ~/path/to/program.e ~/path/to/program.f
and the program will compile and the executable will be created 
in the directory given. If this does not work, or algcompile.ksh 
is missing, you can do
    >gfortran -o program.e program.f fitsiowrap.o 
        /opt/local/lib/libcfitsio.a
Of course, this must be done in the folder that contains 
fitsiowrap.o. This is currently in /Users/cometweb/iraf/.

Once you have compiled the program, you must move it to 
Sites/ and update the zip file.

Enhancement process:

The input image is uploaded to the typical temp folder 
(I don't know the exact location, nor have I had a reason 
to care).

ONLY enhance.php moves the image out of the temp folder, and 
into /Users/cometweb/Sites/img/, AFTER assigning the file an 
id to make the filename unique (NOTE: the ids may not be unique. 
It is possible to have halebopp_231.fits and hartley2_231.fits; 
however there will not be two different halebopp_231.fits files). 
Every image will have an id, even if the file name by itself is 
unique.

Let image.fits be the original image name, with id 1234. The 
following images will appear in the Sites/img/ folder after 
running enhance.php with any of the first four techniques 
(average, median, renorm, and 1/rho):

image_unenhanced_1234.fits
image_unenhanced_1234.png
image_unenhanced_unrotated_1234.fits
image_unenhanced_unrotated_1234.png
image_enhanced_unrotated_techniquename_1234.fits
image_enhanced_unrotated_techniquename_1234.png
image_enhanced_techniquename_1234.fits
image_enhanced_techniquename_1234.png

(techniquename is a specifier for which technique was run; 
will be one of aziav, azimed, aziren, invrho, rvsf, or mosaic)

The first two are the input image, the next the unenhanced 
rho-theta image, then the enhanced rho-theta image, then 
the enhanced (output) xy image. enhance.php will also 
create info.txt, but that more than likely already existed 
in the directory and was just overwritten.

Running enhance.php with image.fits and id 1234 under the 
radially variable spatial filtering technique will give:

image_unenhanced_1234.fits
image_unenhanced_1234.png
image_enhanced_1234.fits
image_enhanced_1234.png

Feel free to do 
>rm *.fits
>rm *.png
in the Sites/img/ folder to remove all uploaded images 
(i.e. all input and output images). There are no other 
important images in that folder. However, this will 
prevent users from being able to see their previously 
uploaded images. Images older than 1 hour are 
automatically removed from the directory every hour on 
the :15 minutes with a cronjob. This cronjob runs 
removeFile.ksh, which may be edited to increase the allowed 
age of files. However, users will likely not be able to 
access the files unless you also increase the lifespan 
of the cookie created in cookie.js. Both are governed by 
the variable IMAGE_DURATION in their respective files -- 
you MUST change both to allow users to keep images longer! 
Note that in removeFile.ksh, IMAGE_DURATION is in hours, 
while in cookie.js, it is in minutes.
