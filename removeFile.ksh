#!/bin/ksh/

#Time images will remain on server
IMG_DURATION=1

find /Users/cometweb/Sites/img -mtime +${IMG_DURATION}h > old_files.txt
echo $(egrep -h '.*\.png$|.*\.fits$|.*\.fit$|.*profile.*' old_files.txt old_files.txt > images_to_remove.txt)

for image_name in $(cat images_to_remove.txt)
do
	echo $image_name
#	echo "----"
	rm -f $image_name
done
