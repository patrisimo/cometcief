
\documentclass[letter]{article}
\usepackage{amsthm,amsfonts,mathtools, amssymb, graphicx}
\usepackage{enumerate}
\usepackage[usenames,dvipsnames]{color}
\usepackage{float}
\usepackage{changepage}
\usepackage[hidelinks]{hyperref}
\usepackage{listings}
\usepackage{geometry}
\geometry{letterpaper, margin=1in}

\usepackage[font={small,color=CornflowerBlue}]{caption}
\usepackage[font={small,color=CornflowerBlue}]{subcaption}
\usepackage{wrapfig}
%\textwidth=6.25in
%\textheight=9.5in
%\headheight=0in
%\headsep=.5in
%\hoffset  -0.75in
%\topmargin -.75in



\newcommand{\mcaption}[1]{\textcolor{CornflowerBlue}{\caption{#1}}}
\newcommand{\msubcaption}[1]{\textcolor{CornflowerBlue}{\subcaption{#1}}}

\setlength{\parindent}{0.0cm}   % Don't indent the paragraphs

\begin{document}
\lstset{basicstyle=\ttfamily,columns=flexible}
\title{CometCIEF Tutorial}
\author{M. Patrick Martin}
\maketitle
This Cometary Coma Image Enhancement Facility (CometCIEF -- pronounced ``Comet Chief'') allows users to have their images enhanced using different enhancement techniques. Each of the techniques requires input parameters in addition to an image to enhance, which we explain in this tutorial. \\

\section{Choosing your technique}

The technique you choose depends on the properties of your image. See the ``An In-depth Description of the Enhancement Routines used in this Facility'' document linked in the Facility for a detailed explanation of the different techniques.\\

This tutorial will describe how to use each technique on our input image, CometCIEF\_test.fits, which is available for download on the Facility from the help page.\\

\section{Image Fields}
The default technique on the Facility is ``Division by Azimuthal Average''; this is for no reason other than alphabetical order. To switch techniques, simply click on the drop-down menu and select a different technique. \\
\begin{wrapfigure}{r}{0.5 \textwidth}
\begin{center}
\vspace{-15pt}
\includegraphics[scale=0.5]{1_filefield.png}
\mcaption{The ``Region to enhance'' fields are automatically filled in when an image is uploaded. Notice the blue help buttons next to each field, which provide useful information regarding those fields.\label{fig:filefield}}
\end{center}
\vspace{-10pt}
\end{wrapfigure}
In order to have an image enhanced, you need an image. We provide CometCIEF\_test.fits for testing; feel free to reference this image while reading along in this tutorial. CometCIEF\_test.fits is a dust (continuum) image of comet Hale-Bopp, 801 pixels by 801 pixels, with the nucleus/optocenter at the center of the field, (400.5,400.5). For the purposes of CometCIEF, the origin is (1,1), not (0,0) as it is in some analysis/processing packages.\\

When you upload the CometCIEF\_test.fits image to the Facility, you will notice that  the ``Region to enhance'' fields are automatically filled in (Figure~\ref{fig:filefield}); these values correspond to the dimensions of the image. We'll leave these be for the moment, and come back once we've enhanced our first image.\\



Additionally, you must specify the coordinates of the optocenter, which for our image is (400.5, 400.5). Type that in the ``(x,y) coordinates of the nucleus/optocenter'' fields.\\


\section{Enhancing our Image}

Once you have the technique selected in the drop-down menu at the top, uploaded a file, and specified the coordinates of the optocenter, a bunch of text will appear and the next two fields -- number of pixels in the $\rho$- and $\theta$-axes -- will be autofilled (Figure~\ref{fig:axisfield}). The text that now appears gives some context to help you fill those fields; the number of pixels in the $\rho$ axis must be less than the first number displayed (566, in this case), and the number of pixels in the $\theta$-axis must be less than 3600. Hover over the question marks or consult the Description document for more information about what these values mean.\\

\begin{figure}[h]
\begin{center}
\vspace{-5pt}
\hspace{-5pt}\includegraphics[scale=0.5]{2_axisfield.png}
\mcaption{The ``Number of pixels'' fields are automatically filled in when the region to enhance and optocenter are set.\label{fig:axisfield}}
\end{center}
\end{figure}

The last field asks for the number of standard deviations to accept in the $\theta$- axis (not shown as a figure in this document), which is explained in the Description document. A good value to use is 3.\\

With everything filled out and no errors, click the ``Submit'' button to have the image enhanced.\\

After a short wait, the Facility will display the output images below the submit button (Figure~\ref{fig:enhanced}). The most important image is the one in the upper right, which is the output enhanced image. To left of that is the orginal image, (CometCIEF\_test.fits in this case), and below are the $\rho$-$\theta$ conversions of the unenhanced and enhanced images.\\
\begin{figure}[h]
\begin{center}
\vspace{-15pt}
\includegraphics[scale=0.39]{3_enhanced.png}
\mcaption{The results of the enhancement.\label{fig:enhanced}}
\vspace{-10pt}
\end{center}
\end{figure}
\section{Reviewing the Results}

Now let's look back at the ``region to enhance'' fields. Let's restrict our field to (200,600) in both the x and y coordinates (Figure~\ref{fig:rhoerror}). Assuming you haven't changed the $\rho$-axis value from earlier, this gives us an error: we've restricted the field too far for our chosen $\rho$-axis value!\\

\begin{figure}[H]
\begin{center}
\vspace{-10pt}
\includegraphics[scale=0.5]{4_rhoerror.png}
\mcaption{The current $\rho$-axis value is too large, throwing an error.\label{fig:rhoerror}}
\vspace{-10pt}
\end{center}
\end{figure}

 To fix this, for example, change this field (number of pixels in $\rho$-axis) to the value of the new radius of the largest circle completely contained in the image (199), which fixes the error. We don't have to adjust the number of pixels in the $\theta$-axis; however, the line directly above that field tells what value corresponds to our specified $\rho$-axis value (1250). Let's leave this field at 2506, and submit again.\\

The output image has changed (Figure~\ref{fig:newoutput})! Compare the positions of the features in figures~\ref{fig:enhanced} and \ref{fig:newoutput}. Figure~\ref{fig:newoutput} corresponds to the innermost region of Figure~\ref{fig:enhanced}, since $\rho$ is 199 pixels in Figure~\ref{fig:newoutput} (instead of 399 pixels).\\

\begin{figure}[H]
\begin{center}
\vspace{-15pt}
\includegraphics[scale=0.4]{5_newoutput.png}
\mcaption{The results of the enhancement after the changes.\label{fig:newoutput}}
\end{center}
\end{figure}
\pagebreak
\section{Next Three Routines}

The next three routines -- Division by Azimuthal Median, Azimuthal Renormalization, and Division by 1/$\rho$ -- are straightforward to use once you've used Division by Azimuthal Average. In fact, Division by Azimuthal Median and Division by 1/$\rho$ do not require any more fields (they also do not use the ``standard deviations'' field), so if you switch to either of them after doing Division by Azimuthal Average, you will be ready to submit the image. Azimuthal Renormalization requires one additional input, which is another standard deviation field. You can use 3 as a default value here as well, and read more about its meaning in the Descriptions document.\\

\section{Radially Variable Spatial Filtering (RVSF)}

The Radially Variable Spatial Filtering technique is different from the other routines, (i.e. it does not use $\rho$-$\theta$ coordinates). Because of this, it does not ask for those values, but rather the values to be used in calculating the kernel size at each pixel. Since an in-depth explanation of how the kernel values impact the enhanced image can be found in the Description document, we will simply use 1, 1, and 0.1 for the A, B, and N values, respectively.\\

The last field asks whether to convert to log-10 space before enhancement. Let's choose ``no'', and submit the image.\\

As seen by the output in Figure~\ref{fig:rvsfim1}, that's not the best enhancement; you can't really see much structure at all. Since our kernel values are pretty small, let's try making all of our values bigger: 4, 4, 0.4, as shown in Figure~\ref{fig:rvsfim2}.\\
\begin{figure}[H]
\begin{center}
\includegraphics{6_rvsp1101.png}
\mcaption{A=1, B=1, N=0.1.\label{fig:rvsfim1}}
\end{center}
\end{figure}



These values now show more structure, but  the spirals are very thick, and could mask the fine structure. This is caused by having a kernel that is too large. To reduce the size of the kernel at the edges, we need to reduce the kernel N term, and to fix the thick spirals near the optocenter we need to reduce the kernel A term. Let's try 2, 4, 0.2, shown in Figure~\ref{fig:rvsfim3}.\\


\begin{figure}[h]
\begin{center}
\begin{subfigure}{0.45\linewidth}
\includegraphics[scale=0.8]{7_rvsp4404.png} 
\msubcaption{A=4, B=4, N=0.4.\label{fig:rvsfim2}}
\end{subfigure} \hspace{10pt}
\begin{subfigure}{0.45\linewidth}
\includegraphics[scale=0.8]{8_rvsp2402.png}
\msubcaption{A=4, B=2, N=0.4\label{fig:rvsfim3}}
\end{subfigure}
\mcaption{RVSF technique run with varying parameters}
\end{center}
\vspace{-20pt}
\end{figure}

This enhancement is much better; you can see the spiral structure very clearly.\\

There isn't a good way to pre-determine good A, B, and N values for an image, so you have to adjust the values based on the output you get. In order to know what values to adjust, remember that changing the A value has the most impact on the kernel size close to the optocenter, while changing the N value has a larger impact further away form the optocenter. The B value impacts the kernel size at every radial distance. In Figure~\ref{fig:rvsfim2} we reduced the N and A terms. However, we could have reduced the B term and left the A and N terms as they were, or even reduced all the terms, and end up with a comparable image. Try it out yourself, with  4, 1, 0.4 and 2, 2, 0.2 for the A, B, N values, and compare the results.\\

\section{Radially Variable Spatial Filtering Mosaic}

The trial-and-error method for finding good A, B, and N values can be tedious, especially at first when you have no idea where to start. To help, there is the option to produce a mosaic of eight images enhanced by RVSF. For the mosaic, you input two values for each A, B, and N, and the Facility will run the technique on all eight combinations and display the output (see Figure~\ref{fig:rvsfmosaic} for a mosaic of A=1 or 4; B=1 or 4, and N=0.1 or 0.4). In this way you may calibrate the kernel terms to best enhance the image. However, because this option runs RVSF eight times,  it takes significantly longer -- running it on the test image takes about three minutes. For this reason, as you refine the kernel terms you may find it easier to switch to the normal RVSF option to quickly check the results from slight changes in kernel values.\\



\section{Viewing Previous Images}

Say you've been experimenting with different techniques, or testing out different kernel values in RVSF, and you want to look at a previous output. Your enhancements are saved on the server for approximately one hour, and you can view your previous outputs by using the drop down menu below the submit button, which will appear when you enhance the first image. Each time you submit an image, the output is added to the bottom of the list. Then, at any point within an hour from when the image was enhanced, you can select the image from the dropdown menu and the output images will be displayed in the output section and be available for download.\\

This feature requires cookies to function, so make sure you have cookies enabled in your browser.



\section{Concluding Remarks and Instructions for Offline Usage}

Now you have used each of the image enhancement techniques offered on CometCIEF, adjusted RVSF parameters to improve the enhancement quality, and reviewed downloading your results. You are now able to use the enhancement Facility to enhance your own cometary coma images -- however if you would like to learn more about the enhancement techniques themselves, then please read the ``In-depth Description of the Enhancement Routines used in this Facility'' accessible from the help page.
\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.9]{9_rvspmosaic14140104.png}
\mcaption{RVSF Mosaic of A=1 or 4, B=1 or 4, and N=0.1 or 0.4.\label{fig:rvsfmosaic}}
\end{center}
\end{figure}
We also allow you to download the executables and source codes for offline use -- these run on Mac OS X 10.8 Mountain Lion. The source codes require two extra files to compile: 
\begin{itemize}
\item fitsiowrap.o, from FITSIOWRAP which is downloadable here: \vspace{-10pt}
\begin{center}
\url{http://www.astro.umd.edu/\~bjw/software/fitsiowrap.html}
\end{center}\vspace{-10pt}
(to create the object file, use the ``-c'' option when compiling), and 
\item libcfitsio.a, downloadable here: \vspace{-10pt}
\begin{center}
\url{http://heasarc.gsfc.nasa.gov/fitsio/}
\end{center}\vspace{-5pt}
 Then compile with:
	gfortran -o outputname.e technique\_source.f fitsiowrap.o libcfitsio.a
\end{itemize}
Of course, replace ``outputname.e'' with the desired output filename, and ``technique\_source.f'' with the name of the source code file. Ensure that you specify the path to where fitsiowrap.o and libcfitsio.a are located on your system. The executable can then be run on the command line, where it prompts for the same information that is asked on the Facility.
\end{document}