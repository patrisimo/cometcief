/*/Users/cometweb/Sites/js/enhance2mosaic.js
 * TABLE OF CONTENTS:
 * 0. Variable initializations
 * 1. Creating/resetting XML objects
 * 2. AJAX submissions and their helper functions
 * 4. Verify input
 * 5. Miscellaneous helper functions
 * 6. Interactions with the HTML page
 */

// - 0 - //
var xmlhttpe = createXMLObject();     //xml object to be used by the enhancement functions
var xmlhttpv = createXMLObject();     //xml object to be used by the verify functions
var div = "aziav";                     //default enhancement technique is "Division by Azimuthal Average
var id;                             //the identification number assigned to the enhanced image and its friends
var imgs = new Array();                //list of the image file names, created when the enhancement technique is run
var index = 0;                        //current index in the heads and imgs arrays
var extension;                        //extension of the uploaded image
var nucerr = 0;                         // flag for if the nucleus field has an error

function begin() {

    setRoutine(div);                     //make sure the tooltips show the appropriate enhancement technique
    $("#algorithm").val(div);             //make sure the form references the correct enhancement technique
    $("."+div).hide();                     //make sure all irrelevant input fields are hidden
    $(".hide").hide();                    //hide all regions that do not need to be shown at the beginning
    var maxsize = num($("#maxSize").val()) / 1000000;
    $(".msize").text(maxsize + " MB");                //  Input the current file size limit into all fields that use it
    //    $(".construction").html("The website is currently undergoing changes. If something is hanky, we're working on it and it will be fixed as soon as possible.<br>")
}

var imXmax;                 //x-length of the image
var imYmax;                 //y-length of the image
var xNuc;                //x location of the nucleus
var yNuc;                //y location of the nucleus
var xMin;                //first x value to process
var xMax;                //last x value to process
var yMin;                //first y value to process
var yMax;                //last y value to process
var rAxis;                //number of pixels in r-axis during enhancement
var thAxis;                //number of pixels in theta-axis during enhancement
var log10;                //whether to convert to log10 space during rvsf
var kerA;                //kernel A in rvsf
var kerB;                //kernel B in rvsf
var kerN;                //kernel N in rvsf
var kerA1;              //first kernel A value in RVSF mosaic
var kerA2;              //second kernel A value in RVSF mosaic
var kerB1;              //first kernel B value in RVSF mosaic
var kerB2;              //second kernel B value in RVSF mosaic
var kerN1;                //first kernel N value in RVSF mosaic
var kerN2;              //second kernel N value in RVSF mosaic
var errors = 1;            //any errors in the form? 1 means no errors
var filecheck=false;            //has the current file been checked?
var incRad;                //number of pixels in the largest incomplete circle in the region centered on the nucleus
var comRad;                 //Communist pig! Number of pixels in the largest complete circle in the region centered on the nucleus
var manRad;                 //tracks if the user has manually entered the number of pixels wanted in the r-axis
var manCirc;                 //tracks if the user has manually entered the number of pixels wanted in the theta axis
var fileset=false;            //has a file been uploaded?
var reject;                 //standard deviations to reject
var renorm;                 //standard deviations to accept in azimuthal renormalization
var filename;                //name of the file uploaded
var filesize;                //size of the file in bytes
var filex;                //x-length of the image
var filey;                //y-length of the image
var filext;                //file extension of the uploaded file
var logerr;                //flag for negative values, which screws with rvsf

// - 1 - //

/**
 * Creates an XML object to use in the AJAX submissions
 */
function createXMLObject() {
    var xmlobject;
    if(window.XMLHttpRequest) { //works for modern browsers
        xmlobject=new XMLHttpRequest();
    } else { //works for internet explorer
        xmlobject=new ActiveXObject("Microsoft.XMLHTTP");
    }
    if(xmlobject == null) {
        $("#noxml").text("The browser you are using is not compatible with this program. Please use the most recent version of Safari, Internet Explorer, Opera, Google Chrome, or Mozilla Firefox");
    }
    return xmlobject;
}
/**
 * Resets both xml objects, effectively stopping the ajax function calls
 */
function resetXML() {
    xmlhttpe = createXMLObject(); //reset the xmlhttpe object
    xmlhttpv = createXMLObject(); //reset the xmlhttpv object
    $("#submit").val("Submit"); //reset the submit button
    $("#submit").attr("onClick", "verifySubmit(this.form)");
} 
/**
 * Main submission. Runs verify() once more, to ensure there are no errors.
 * If there are no errors, sends the form to the enhancement function
 */
function verifySubmit(form) {
    filecheck=false;
    verify(form); //verify the form
    if(!fileset) { //make sure a file has been uploaded
        $("#fileErr").text("Please upload a file");
        errors *= 2;
    }
    if(errors != 1) { //notify the user of any errors
        var base = $("#submitErrors").html();
        if(base.length > 0) {
            $("#submitErrors").html("There are still errors in the form:<br>&nbsp;&nbsp;&nbsp;&nbsp;" + base);
            $("#submitErrors").show();
        }
    }
    else { //if there are no problems, begin the enhancement
        submitForm(form);
    }
}

/**
 * Will only be run when there are no errors in the form. Sends the form to the php program to perform the enhancement.
 */
function submitForm(form) {
    $("#submit").val("Working... click to cancel"); //change the button so the user knows the enhancement algorithm is working
    $("#submit").attr("onClick", "resetXML()"); //allow the user to cancel the enhancement and try again
    var fd = new FormData(form);
    xmlhttpe.open("POST", "http://" + document.domain + "/cometimen/img/enhance.php", true);
    xmlhttpe.send(fd);
    xmlhttpe.onreadystatechange=function() {
        if(xmlhttpe.readyState == 4 && xmlhttpe.status ==200) {
            id  = xmlhttpe.responseText;
            if(id == "error") {
                uploadError();
            } else {
                imgs = new Array();
                imgs = id.split("&");
                addUpload();
                setUpImages(imgs); //display the images
            }
            $("#submit").val("Submit"); //fix the button
            $("#submit").attr("onClick", "verifySubmit(this.form)"); 

        } else if(xmlhttpe.readyState == 4) {
            $("#submitErrors").text("XML error " + xmlhttpe.status + " occurred: " + xmlhttpe.statusText);
            $("#submitErrors").show();
            $("#submit").val("Submit"); //fix the button
            $("#submit").attr("onClick", "verifySubmit(this.form)");
        }
    }
}
/*
 * Gets information from the uploaded file. Namely the file extension, its size (bytes), width, height.
 */
function fileInfo(form) {
    if(!filecheck) {
        if ( form.file.files[0] != null && form.file.files[0].size > form.maxSize.value ) {
          $("#fileErr").text("File is " + form.file.files[0].size + " bytes, which is above the maximum size of " + form.maxSize.value + " bytes.");
          errors *= 5;
          return;
        }
        var fd = new FormData(form);
        xmlhttpv.open("POST", "http://" + document.domain + "/cometimen/verifyFile.php", true);
        xmlhttpv.send(fd);

        xmlhttpv.onreadystatechange=function() {
            if(xmlhttpv.readyState == 4 && xmlhttpv.status ==200) {
                info = xmlhttpv.responseText;
                info = info.toString();
                verifyFile(info);
                filecheck=true;
            } else if(xmlhttpv.readyState == 4) {
                $("#fileErr").text("An error (error code " + xmlhttpv.status + ") occurred when reading the image: " + xmlhttpv.statusText);
            }
        }
    } else {
        verifyFile(info);
    }
}

/*
 * Verifies the information received from the form. 
 */
function verifyFile(info) {
    fileset = (info != null) && (info.indexOf("problem") != 0); //make sure something actually was returned, and that there wasn't a problem
    if(fileset) { //since the fileinfo worked without problems, a file was uploaded
        inf = info.split("\n"); //divide the return into lines
        filext = inf[0]; //the first line contains the file extension
        filesize = num(inf[2]); //the third line is the size of the file in bytes
        filex = num(inf[3]); //the fourth line is the width of the file
        filey = num(inf[4]); //the fifth line is the height of the file
        filename = inf[5]; //the sixth line is the name of the file
        logerr = inf[6]; //the seventh line will be true if there are negative values
        if(inf[0] != "fits" && inf[0] != "fit") { //the file must be a fit(s) image
            $("#fileErr").text("File must be *.fits or *.fit format");
            errors *=3;
            return;
        }
        else if(num(inf[1]) > num(inf[2])) { //the second line is the maximum file size. So the file size must be less than the maximum file size
            $("#fileErr").text("File is " + inf[1] + " bytes, which is above the maximum size of " + inf[2] + " bytes");
            errors *= 5;
            return;
        }
        if(xMin == null || isNaN(xMin)) { //if the lower xbound has not been set, then set it to 1
            xMin = 1;
            $("#xMin").val(1);
        }
        else { //otherwise, make sure the input bound is consistent with the input image (i.e., it is not too far out)
            if(nullGreater(xMin,filex)) {
                errors *= 7;
                $("#xLimErr").text("This is not in the image");
            }
        }
        if(yMin == null || isNaN(yMin)) { //if the lower ybound has not been set, then set it to 1
            yMin = 1;
            $("#yMin").val(1);
        }
        else { //otherwise, make sure the input bound is consistent with the input image (i.e., it is not too far out)
            if(nullGreater(yMin,filey)) {
                errors *= 11;
                $("#yLimErr").text("This is not in the image");
            }
        }
        if(xMax == null || isNaN(yMin)) { //if the upper xbound has not been set, then set it to the width of the image
            xMax = num(inf[3]);
            $("#xMax").val(num(xMax));
        }
        else { //otherwise, make sure the input bound is consistent with the input image (i.e. it is not too far out)
            if(xMax > num(inf[3])) {
                errors *= 13;
                $("#xLimErr").text("This is not in the image");
            }
        }
        if(yMax == null || isNaN(yMax)) { //if the upper ybound has not been set, then set it to the height of the image
            yMax = num(inf[4]);
            $("#yMax").val(yMax);
        }
        else { //otherwise, make sure the input bound is consistent with the input image (i.e. it is not too far out)
            if(yMax > num(inf[4])) {
                errors *= 17;
                $("#yLimErr").text("This is not in the image");
            }
        }
    }
    else { 
        errors *= 23; //if there is a problem with the file, set errors to true
        $("#fileErr").text("Error processing file: make sure a valid file has been uploaded.");
    }
}

/*
 * Displays the images and prepares the forms for download
 */
var images = 4;
function setUpImages(imgs) {
    var heads = new Array();
    if(imgs.length == 2) {
        heads[0] = "unxy";
        heads[1] = "enxy";
        $(".rhotheta").hide();
        $("#nonmosaic").show();
        $("#mosaictable").hide();
    } else if(imgs.length == 8) {
        for(i = 0;i<8;i++) {
            heads[i] = (Math.floor(i / 4) + 1) + "" + (Math.floor((i % 4) / 2) + 1) + "" + ((i % 2) + 1);
        }
        var kerterms= imgs[0].split('_');
        var kersize=kerterms.length;
        // image names will be blahblahblah_rvsf_enhanced_a_b_c_id.fit(s)
        //                                                4 3 2 1
        if (kersize < 4) {
            alert("Problem with image name: Only " + kersize + " segments found, should be at least four.\n'"+imgs[0]+"'");
           }
        $(".A1").text(kerterms[kersize - 4].substring(1));
        $(".B1").text(kerterms[kersize - 3].substring(1));
        $(".N1").text(kerterms[kersize - 2].replace("-",".").substring(1));

        kerterms = imgs[7].split("_");
        kersize = kerterms.length;

        $(".A2").text(kerterms[kersize - 4].substring(1));
        $(".B2").text(kerterms[kersize - 3].substring(1));
        $(".N2").text(kerterms[kersize - 2].replace("-",".").substring(1));

        $("#mosaictable").show();
        $("#nonmosaic").hide();
    } else {
        heads[0] = "unxy";    //the unenhanced, xy image (input)
        heads[1] = "unrt";    //the unenhanced, rho-theta image
        heads[2] = "enrt";    //the enhanced, rho-theta image
        heads[3] = "enxy";    //the enhanced, xy image (output)
        $(".rhotheta").show();
        $("#nonmosaic").show();
        $("#mosaictable").hide();
    }


    $("#images").show(); //unhide the image area
    for(var i=0;i<imgs.length;i++) {
        $("#"+heads[i]).html("<img class=\"img\" src=\""+imgs[i] +".png\" onerror=\"img_error("+heads[i]+")\" >"); //show the image
        //    $('input:hidden[name="file"]', "#download"+heads[i]).val(imgs[i] + "." + filext); //give each form the name of its file    
        $('input:hidden[name="file"]', "#download"+heads[i]).val(imgs[i] + ".fits"); //give each form the name of its file    
    }

}

/*
 * Deals with hiding cells and displaying an error message when an image fails to load.
 */

function img_error(id) {
    $("#"+id).html("There was an error loading this image. If this image was selected using the drop-down table at the top of this page, it is possible that the image has been deleted. Otherwise, please try resubmitting the image.");
}

/*
 * Deals with showing an error message when the enhancement routine encounters a problem
 */
function uploadError() {
    $("#submitErrors").text("An error occurred enhancing your image. Please contact us if the error is not resolved.");
    $("#submitErrors").show();
}

/*
 * Ensures all the input information is self-consistent and meets the requirements of the enhancement algorithms
 */
function verify(form) {
    //get the parameters from the form
    xNuc = num(form.x.value); 
    yNuc = num(form.y.value);
    xMin = num(form.xmin.value);
    yMin = num(form.ymin.value);
    xMax = num(form.xmax.value);
    yMax = num(form.ymax.value);
    rAxis = num(form.raxis.value);
    thAxis = num(form.thaxis.value);
    log10 = num($('input[name=log10]:checked').val());
    kerA = num(form.kernelA.value);
    kerB = num(form.kernelB.value);
    kerN = num(form.kernelN.value);
    kerA1 = num(form.kernelA1.value);
    kerA2 = num(form.kernelA2.value);
    kerB1 = num(form.kernelB1.value);
    kerB2 = num(form.kernelB2.value);
    kerN1 = num(form.kernelN1.value);
    kerN2 = num(form.kernelN2.value);


    fileset = form.uploadedfile.value ? "true" : "false";
    reject = num(form.reject.value);
    renorm = num(form.renorm.value);
    $(".error").text(""); //clear the errors field
    $(".warning").text(""); //clear warnings
    $("#submitErrors").hide();
    errors = 1;
    fileInfo(form);

    if(div == "aziav") 
    {
        checkNucleus();
        checkXYBounds();
        calculateRadialValues();
        autofillRadiusAndCircumference();
        checkRadiusPixels();
        checkCircumferencePixels();
        checkSimpleRejection();
    }
    else if(div == "azimed") 
    {
        checkNucleus();
        checkXYBounds();
        calculateRadialValues();
        autofillRadiusAndCircumference();
        checkRadiusPixels();
        checkCircumferencePixels();
    }
    else if(div == "aziren")
    {
        checkNucleus();
        checkXYBounds();
        calculateRadialValues();
        autofillRadiusAndCircumference();
        checkRadiusPixels();
        checkCircumferencePixels();
        checkSimpleRejection();
        checkRenormalizationRejection();
    }
    else if(div == "invrho")
    {
        checkNucleus();
        checkXYBounds();
        calculateRadialValues();
        autofillRadiusAndCircumference();
        checkRadiusPixels();
        checkCircumferencePixels();
    }

    else if(div == "rvsf")
    {
        checkNucleus();
        checkXYBounds();
        calculateRadialValues();
        autofillRadiusAndCircumference();
        checkKernelA();
        checkKernelB();
        checkKernelN();
        checkLog10Space();
    }
    else if(div == "mosaic")
    {
        checkNucleus();
        checkXYBounds();
        checkKernelAs();
        checkKernelBs();
        checkKernelNs();
        checkLog10Space();
    }

}

/* ***** This makes sure the input values for the nucleus/optocenter are set (submission error) and are not outside of the image dimensions or the user-specified dimensions ***** */

function checkNucleus() {
    nucerr = 0;
    if(!(xNuc == null ||  yNuc == null)) {//if either isn't set yet then they will be null. This block will only run when both the x and y location of the nucleus are set.
        if(xNuc < 1 || nullGreater(xNuc,xMax) || nullLess(xNuc,xMin)) { //not in the range of the bounds
            $("#nucErr").text("Nucleus/optocenter coordinates not consistent with X-limits specified");
            errors *= 29;
            nucerr = 1;
        }
        if(yNuc < 1 || nullGreater(yNuc,yMax) || nullLess(yNuc,yMin)) { //not in the range of the bounds
            $("#nucErr").text("Nucleus/optocenter coordinates not consistent with Y-limits specified");
            errors *= 31;
            nucerr = 1;
        }
    }
    if(xNuc == null) {
        errors *= 37;
        nucerr = 1;
        errorMessage("X coordinate of nucleus/optocenter not set");
    }
    if(yNuc == null) {
        errors *= 43;
        nucerr = 1;
        errorMessage("Y coordinate of nucleus/optocenter not set");
    }
}


/* ***** This makes sure the min X and Y values and the max X and Y values are:
   - set
   - non-negative
   - such that the min is less than the max
   - such that the total area is less than 10,800,000

 ***** */
function checkXYBounds() {
    if(nullLess(xMin,1)) { //make sure the boundaries are in the image
        $("#xLimErr").text("This is not in the image");
        errors *= 47;
    }

    if(nullGreater(xMin,xMax)) {
        $("#xLimErr").text("Min must be less than the max");
        errors *= 53;
    }
    if(nullLess(yMin,1)) {
        $("#yLimErr").text("This is not in the image");
        errors *= 59;
    }
    if(nullGreater(yMin,yMax)) {
        $("#yLimErr").text("Min must be less than the max");
        errors *= 61;
    }
    if(nullLess(10800000,(xMax-xMin)*(yMax-yMin))) {
        $("#yLimErr").text("Total number of pixels to enhance must be less than 10,800,000");
        errors *= 41;
    }
    if(xMin == null) {
        errors *= 67;
        errorMessage("Minimum x coordinate not set");
    }
    if(xMax == null) {
        errors *= 71;
        errorMessage("Maximum x coordinate not set");
    }
    if(yMin == null) {
        errors *= 73;
        errorMessage("Minimum y coordinate not set");
    }
    if(yMax == null) {
        errors *= 79;
        errorMessage("Maximum y coordinate not set");
    }

}

/* ***** Calculates the radius and circumference of the largest circles completely and partially contained in the image ***** */

function calculateRadialValues() {
    //Radius of largest circle partially contained
    incRad = Math.floor(Math.sqrt( Math.pow(Math.max(xMax - xNuc, xNuc-xMin),2) + Math.pow(Math.max(yMax - yNuc, yNuc-yMin),2)));

    //Radius of largest circle completely contained
    var list =[xNuc-xMin, yNuc-yMin, xMax-xNuc, yMax-yNuc]; //list of lengths from nucleus to side
    comRad = whole(min(list));    //largest complete circle's radius is the minimum of that list

    //Circumference of the largest circle partially contained
    incCirc = Math.floor(Math.PI * incRad*2); 

    //Circumference of the largest circle completely contained
    comCirc = Math.floor(Math.PI * comRad*2);
}

/* ***** Auto-filling the radius and circumference fields for Azimuthal Average, Azimuthal Median, Azimuthal Renormalization, and Division by 1/rho ***** */

function autofillRadiusAndCircumference() {
    if(xNuc != null && yNuc != null && xMax != null && xMin != null && yMin != null && yMax != null && nucerr == 0) { //once the nucleus and boundaries have been set and there are no errors
        $("#radii").html("Radius of the largest circle centered on the nucleus/optocenter and partially contained in the image: " + incRad + "<br> Radius of the largest circle centered on the nucleus/optocenter and completely contained in the image: " + comRad); //tell the largest complete and incomplete radii
        $("#radii").show(); //this div is set to be hidden by default
        if(rAxis == null || isNaN(rAxis)) { //if the radius field is blank, fill it with the largest complete radius
            if(comRad > 0) {
                $("#rAx").val(comRad);
                rAxis = comRad;
            }
        }

        //tell the largest complete and incomplete circumferences as well as the circumference of the current circle, based on the radius selected
        $("#circ").html(
                "Circumference of the largest circle centered on the nucleus/optocenter and partially contained in the image: "
                + incCirc + 
                "<br> Circumference of the largest circle centered on the nucleus/optocenter and completely contained in the image: " 
                + comCirc + 
                "<br> Circumference of the circle corresponding to &rho;-axis value selected: " 
                + Math.floor(2* Math.PI * rAxis)); 
        $("#circ").show();
        if(thAxis == null || isNaN(thAxis)) { //if the circumference field is bland, fill it with the circumference of the largest complete radius
            thAxis = Math.min(comCirc,num($("#thAxisMax").val()));
            if(thAxis > 0) {
                $("#thAx").val(thAxis);
            }
        }
    }
}
/* ***** Verifying the radial-axis value, making sure it is at most equal to the largest incomplete circle's radius, at least 1, and set ***** */
function checkRadiusPixels() {
    if(rAxis > incRad) { //you can't have more pixels in the r-axis than there are pixels in the largest incomplete circle's radius
        errors *= 83;
        $("#rAxErr").text("Must be less than the radius of the largest incomplete circle: " + incRad);
    }
    if(nullLess(rAxis,1)) { //this can't be nonpositive
        errors *= 89;
        $("#rAxErr").text("Must be a positive whole number");
    }
    if(rAxis == null) {
        errors *= 97;
        errorMessage("Pixels in &rho; axis not set");
    }

}
/* ***** Verifying the circumference-axis value, making sure it is not more than the maximum, not less than 1, and set ***** */

function checkCircumferencePixels() {
    if(thAxis > Math.max(num($("#thAxisMax").val()))) { //there is currently a cap on the number of pixels allowed in the theta axis
        errors *= 101;
        $("#thAxErr").text("To limit file size, pixel count is capped at  " + $("#thAxisMax").val());
    }
    if(nullLess(thAxis,1)) { //this can't be nonpositive
        errors *= 103;
        $("#thAxErr").text("Must be a positive whole number");
    }
    if(thAxis == null) {
        errors *= 107;
        errorMessage("Pixels in &theta; axis not set");
    }
}

/* ***** Make sure the sigma rejection for Azimuthal Average and Azimuthal Renormalization is set ***** */
function checkSimpleRejection() {
    if(reject == null) { //if the sigma rejection is not set, and the enhancement algorithm requires a sigma, then there is a problem
        errors *= 109;
        errorMessage("Standard deviations for rejection not set.");
    }
}

/* ***** Make sure the sigma rejection for Azimuthal Renormalization is set ***** */
function checkRenormalizationRejection() {
    if(renorm == null) { 
        errors *= 113;
        errorMessage("Standard deviation for minimum and maximum pixels not set.");
    }
}

/* ***** Make sure the kernel A value is set and greater than zero ***** */
function checkKernelA() {
    if(kerA == null) {
        errors *= 127;
        errorMessage("Kernel A not set.");
    } else if(kerA <= 0) {
        errors *= 131;
        $("#kernelAErr").text("This must be strictly positive.");
    }
}

/* ***** Make sure the kernel B value is set and greater than zero ***** */
function checkKernelB() {
    if(kerB == null) {
        errors *= 137;
        errorMessage("Kernel B not set");
    } else if(kerB <= 0) {
        errors *= 139;
        $("#kernelBErr").text("This must be strictly positive.");
    }
}

/* ****** Make sure the kernel N value is set and greater than zero ***** */
function checkKernelN() {
    if(kerN == null) {
        errors *= 149;
        errorMessage("Kernel N not set");
    } else if(kerN <= 0) {
        errors *= 151;
        $("#kernelNErr").text("This must be strictly positive.");
    }
}

/* ***** Make sure the kernel A values are set and greater than zero ***** */
function checkKernelAs() {
    if(kerA1 == null) {
        errors *= 157;
        errorMessage("First kernel A not set.");
    } else if(kerA1 <= 0) {
        errors *= 163;
        $("#kernelAsErr").text("This must be strictly positive.");
    }
    if(kerA2 == null) {
        errors *= 167;
        errorMessage("Second kernel A not set.");
    } else if(kerA2 <= 0) {
        errors *= 173;
        $("#kernelAsErr").text("This must be strictly positive.");
    }
}

/* ***** Make sure the kernel B values are set and greater than zero ***** */
function checkKernelBs() {
    if(kerB1 == null) {
        errors *= 179;
        errorMessage("First kernel B1 not set");
    } else if(kerB1 <= 0) {
        errors *= 181;
        $("#kernelBsErr").text("This must be strictly positive.");
    }
    if(kerB2 == null) {
        errors *= 191;
        errorMessage("Second kernel B not set");
    } else if(kerB2 <= 0) {
        errors *= 193;
        $("#kernelBsErr").text("This must be strictly positive.");
    }
}

/* ***** Make sure the kernel N values are set and greater than zero ***** */
function checkKernelNs() {
    if(kerN1 == null) {
        errors *= 197;
        errorMessage("First kernel N not set");
    } else if(kerN1 <= 0) {
        errors *= 199;
        $("#kernelNsErr").text("This must be strictly positive.");
    }
    if(kerN2 == null) {
        errors *= 211;
        errorMessage("Second kernel N not set");
    } else if(kerN2 <= 0) {
        errors *= 223;
        $("#kernelNsErr").text("This must be strictly positive.");
    }
}

/* ***** Ensuring a log10 option has been chosen ***** */
function checkLog10Space() {
    if(log10 == null) {
        errors *= 227;
        errorMessage("log 10 option not set");
    }
    if(log10) {
        $("#log10warn").html("If the input image has negative values, then the log will not be taken.<br>The output image header will indicate whether the log was taken or not.");
        $("#log10warn").show();
    }
}

/*
 * Adds text to the error message at the bottom of the form. These errors will only show if the user is trying to submit the form. If this function is only being run because the user has updated information, this will not show.
 */
function errorMessage(text) {
    var base = $("#submitErrors").html();
    base = base + "<br>&nbsp;&nbsp;&nbsp;&nbsp;" + text;
    $("#submitErrors").html(base);
}

/*
 * Returns a value as a number if it is one, or null if it is not
 */
function num(num) {
    var dum = parseFloat(num);
    if(isNaN(dum)) return null;
    else return dum;
}
/*
 * Returns a value as an integer if is one, or null if it is not
 */
function whole(num) {
    var dum = parseInt(num);
    if(isNaN(dum)) return null;
    else return dum;
}
/*
 * Returns the minimum value of a list through recursion
 */
function min(list) {
    return mins(list, 0);
}
/*
 * Recursive call, compares the initial value to the minimum of the sublist to the right.
 */
function mins(list, index) {
    if(index == list.length -1) return list[index];
    else {
        return Math.min(list[index],mins(list, index+1));
    }
}
/*
 * Returns true only if a < b
 */
function nullLess(a,b) {
    if(a == null || b == null || a>=b) {
        return false;
    }
    else {
        return true;
    }
}
/*
 * Returns true only if a > b
 */
function nullGreater(a,b) {//returns false only if a <= b
    if(a == null ||  b == null ||  a<=b) {
        return false;
    }
    else {
        return true;
    }
}


/*
 * Resets all the fields to their default settings, hides images
 */
function clear() {
    $("#file").val("");
    $("#xnuc").val("x");
    $("#ynuc").val("y");
    $("#xmin").val("min");
    $("#xmax").val("max");
    $("#ymin").val("min");
    $("#ymax").val("max");
    $("#thaxis").val("");
    $("#raxis").val("");
    $("#reject").val("");
    $("#renorm").val("");
    $("#radii").val("");
    $("#circ").val("");
    errors = 1;

}

/* 
 * Updates the tooltips so that they reference the appropriate enhancement algorithm.
 */
function setRoutine(routine) {
    var name,tech;
    images = 4;
    text = "routine converts the input image to";
    if(routine == "aziav") {
        name = "division by azimuthal average";
        tech = "t1";
    }
    if(routine == "invrho") {
        name = "division by 1/&rho; profile";
        tech = "t2";
        text = "option provides unenhanced and enhanced versions of the image in";
    }
    if(routine == "azimed") {
        name = "division by azimuthal median";
        tech = "t3";
    }
    if(routine == "aziren") {
        name = "azimuthal renormalization";
        tech = "t4";
    }
    if(routine == "rvsf") {
        name = "radially variable spatial filtering";
        tech = "t5";
        images = 2;
    }
    if(routine == "mosaic") {
        name = "radially variable spatial filtering";
        tech = "t5";
        images = 8;
    }
    $(".routine").html(name);
    $(".rtntext").html(text);
    $(".help").attr("href","techniques.html#"+tech);
}
//Assorted JQuery functions
$(document).ready(function(){
        begin();
        $("#technique").change(function(){ //update when the technique is changed, namely the tooltips and the algorithm
            $("."+div).show();
            div = $(this).val();
            setRoutine(div);
            $("#algorithm").val(div);
            $("."+div).hide();
            });
        $(".small").click(function(){ //automatically clear the field when clicked
            $(this).val("");
            });    
        $(".small").change(function(){ //whenever new information is input, verify the form is ok
            verify(this.form);

            });
        $(".whole").change(function(){ //some inputs can only have whole numbers
            var temp = whole($(this).val());
            $(this).val(temp);
            });            
        $(".num").change(function(){ //some inputs can only be numbers
            var temp = num($(this).val());
            $(this).val(temp);
            });
        $('[name="log10"]').change(function(){ //update form when information is input
                verify(this.form);
                });
        $("#file").change(function(){ //when the user uploads a file
                fileset = true;
                filecheck = false;
                verify(this.form);
                });
})
