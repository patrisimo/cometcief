var imgsarray = []; //imgs array is a 2d array, where every column is the imgs array for a previous upload
var title = []; //title is a 2d array of the title of previous uploads, instead of just using the filename
var IMAGE_DURATION = 60;
/**
 * Create a cookie in the user's browser
 */
function bakeCookie(name, value, expire_minutes) {
	"use strict";
	var exdate = new Date(), cookie_value = escape(value);
	exdate.setTime(exdate.getTime() + (expire_minutes * 60 * 1000));
	cookie_value += "; expires=" + exdate.toUTCString();
	document.cookie = name + "=" + cookie_value;
}

/**
 * Remove a cookie with the given name
 */
function delCookie(name) {
	document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

/**
 * Get the data of an already-existing cookie
 */
function getCookie(c_name) {
	"use strict";
	var c_value = document.cookie, c_start = c_value.indexOf(" " + c_name + "="), c_end;
	if (c_start === -1) {
		c_start = c_value.indexOf(c_name + "=");
	}
	if (c_start === -1) {
		c_value = null;
	} else {
		c_start = c_value.indexOf("=", c_start) + 1;
		c_end = c_value.indexOf(";", c_start);
		if (c_end === -1) {
			c_end = c_value.length;
		}
		c_value = unescape(c_value.substring(c_start, c_end));
	}
	return c_value;
}

// use this one on an image filename to get the digits at the end
function getImageID(name) {
    "use strict";
    var namesegs = name.split("_");
    return namesegs[namesegs.length-1];
}



//use this on the first of the files to get filename_technique_unenhanced_idnum.fits
// deprecated
function getBaseName(name) {
	"use strict";
	var newname = name.split("_"), pasted, i;
	for (i = 0; i < newname.length - 2; i++) {
		pasted += newname[i] + "_";
	}
    pasted = newname.slice(0,newname.length-2).join("_");
	return pasted.substring(4); //omit the img/ at the beginning
}
/**
 * Update the drop down menu that allows users to see previous uploads
 */
function updateSelector() {
	"use strict";
	var selection = "<select id=\"reshow\" name=\"reshow\" onchange=\"showPrevious()\">", i;
	for (i = 0; i< imgsarray.length ; i++) {
		selection += "<option value=\"" + i + "\"";
        if (i == imgsarray.length - 1)
            selection += " selected=true";
        selection += ">" + title[i][0] + "</option>";
	}
	selection += "</select>";
	$("#welcome").html("Your previous uploads have been saved, listed in the order they were enhanced.<br>Select one below to view them and download.<br>" + selection);
	$("#welcome").show();

}
/**
 * Create a cookie corresponding to the most recent upload and update the drop down menu with this upload
 * 'filename' is the original filename of this upload
 */
function addUpload() {
	//get all the image file names in one string
	"use strict";
    var titlepart = filename + " " + div + " (" + getImageID(imgs[0]) + ")";
	var encodedFiles = titlepart + "&"+filext+"&";
	var i, index, j;
	for (i = 0; i < imgs.length; i++) {
		encodedFiles += imgs[i];
		if (i != imgs.length -1) {
			encodedFiles += "&";
		}
	}
	//increment the title
	index = num(getCookie("imagesavailable")) + 1;
	bakeCookie("imagesavailable", index, IMAGE_DURATION);
	//make a new cookie
	bakeCookie("image" + index, encodedFiles, IMAGE_DURATION);
	j = imgsarray.length;
	imgsarray[j] = imgs;
	title[j] = new Array(titlepart,filext);
	updateSelector();
}



//allfiles will look like title&ext&unenhancedxy&unenhancedrt&enhancedrt&enhancedxy&##unenhancedxy...
/**
 * Checks for any cookies, and if there are, displays all the uploads that are available
 */
function checkCookie() {
	"use strict";
	var allfiles = "", highindex, num_files = 0, i, j, k, temp, filesarray, text;
	highindex = num(getCookie("imagesavailable")); //the root cookie tells how many cookies there might be
	if (highindex !== null && highindex !== "") { //lets get all the cookie information
		allfiles = ""; 
		num_files = 0;
		for (i = 1; i <= highindex; i++) { //get all the cookie data, providing the appropriate splitters
			temp = getCookie("image" + i);
			if (temp !== null && temp !== "") {
				allfiles += "&##&" + temp;
				num_files += 1;
			}
		}
		allfiles = allfiles.substring(1);
		filesarray = allfiles.split("&"); //split on the inter-file splitter, &
		imgsarray = [];
		j = 0;
		for (i = 0; i < filesarray.length -1; ) {

			if (filesarray[i] === "##") { //the inter-upload splitter is ##
				imgsarray[j] = [];
				i++;
				title[j] = [];
				title[j][0] = filesarray[i];
				i++;
				title[j][1] = filesarray[i];
				i++;
			}
			for (k = 0; i < filesarray.length && filesarray[i] != "##"; k++) { //add all image file names to the array
				imgsarray[j][k] = filesarray[i];
				i++;
			}
			j++;
		}
		imgs = imgsarray[j - 1];
		filext = title[j-1][1];
		updateSelector();
		setUpImages();
	}
}

function showPrevious() {
	var index = $("#reshow").val();
	imgs = imgsarray[index];
	filext = title[index][1];
	setUpImages(imgs);
//    location.href = "#";
//    location.href = "#images";
}

$(document).ready(function() {
		"use strict";
		checkCookie();
		});
